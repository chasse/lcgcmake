* Herwig3-7.0.4 and gcc7:

```
LCG_93/install/MCGenerators/thepeg/2.0.4/x86_64-slc6-gcc7-opt/include/ThePEG/Utilities/Selector.h:131:50: error: ISO C++1z does not allow dynamic exception specifications
   T & select(double rnd, double * remainder = 0) throw(range_error)
                                                  ^~~~~
LCG_93/install/MCGenerators/thepeg/2.0.4/x86_64-slc6-gcc7-opt/include/ThePEG/Utilities/Selector.h:140:30: error: ISO C++1z does not allow dynamic exception specifications
   T & operator[](double rnd) throw(range_error) { return select(rnd) }
                              ^~~~~
LCG_93/install/MCGenerators/thepeg/2.0.4/x86_64-slc6-gcc7-opt/include/ThePEG/Utilities/Selector.h:153:62: error: ISO C++1z does not allow dynamic exception specifications
   const T & select(double rnd, double * remainder = 0) const throw(range_error)
                                                              ^~~~~
LCG_93/install/MCGenerators/thepeg/2.0.4/x86_64-slc6-gcc7-opt/include/ThePEG/Utilities/Selector.h:162:42: error: ISO C++1z does not allow dynamic exception specifications
   const T & operator[](double rnd) const throw(range_error) { return select(rnd) }
                                          ^~~~~
LCG_93/install/MCGenerators/thepeg/2.0.4/x86_64-slc6-gcc7-opt/include/ThePEG/Utilities/Selector.h:177:28: error: ISO C++1z does not allow dynamic exception specifications
   T & select(RNDGEN & rnd) throw(range_error) {
                            ^~~~~
LCG_93/install/MCGenerators/thepeg/2.0.4/x86_64-slc6-gcc7-opt/include/ThePEG/Utilities/Selector.h:197:40: error: ISO C++1z does not allow dynamic exception specifications
   const T & select(RNDGEN & rnd) const throw(range_error) {
                                        ^~~~~
```