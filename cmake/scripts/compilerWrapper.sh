#!/bin/bash

# Accepting following parameters:
#  $1: compiler version
#  $2: architecture (avx, avx2, sse2...)

if [ $# -ge 2 ]; then
  COMPILER=$1; shift
  ARCHITECTURE=$1; shift
else
  echo "$0: expecting 2 arguments: [compiler] [architecture]"
  return
fi

IFS='+ ' read -r -a array <<EOF
$ARCHITECTURE
EOF
for element in ${array[@]}
do
    arch+=m$element+
    arch_params+="-m$element "
done

CXXOPTIONS=${arch%?}
WRAPPER_CXX_FLAGS=${arch_params}
WRAPPER_C_FLAGS=${arch_params}

# Get compiler version number
comp_vers=`echo $COMPILER | grep -Eo "[0-9]+"`

#### Instruction set
#export LCG_EXT_ARCH="-march=core-${ARCHITECTURE}"
export ARCHITECTURE
export CXXOPTIONS
export WRAPPER_CXX_FLAGS
export WRAPPER_C_FLAGS

# Reset the variable
unset LCG_CXX_FLAGS

export LCG_CPP11="FALSE"
export LCG_CPP1Y="FALSE"
export LCG_CPP14="FALSE"
export LCG_CPP17="FALSE"

#handling of C++ flags
# enable it only for gcc 4.6 and greater
function cxx11_flag {
  if [[ $COMPILER  == *gcc* ]]; then
    if [ $comp_vers -gt 46 ]; then
      export LCG_CXX_FLAGS="-std=c++11"
      export LCG_CPP11="TRUE"
      return 0
    fi
  elif [[ $COMPILER == *clang* ]]; then
    export LCG_CXX_FLAGS="-std=c++11"
    export LCG_CPP11="TRUE"
    return 0
  fi
  return 1
}

#handling of C++1y
# enable it only for gcc 4.9 and greater
function cxx1y_flag {
  if [[ $COMPILER  == *gcc* ]]; then
    if [ $comp_vers -gt 48 ]; then
      export LCG_CXX_FLAGS="-std=c++1y"
      export LCG_CPP1Y="TRUE"
      return 0
    fi
  fi
  return 1
}
function cxx17_flag {
  if [[ $COMPILER  == *gcc* ]]; then
    if [ $comp_vers -gt 72 ]; then
      export LCG_CXX_FLAGS="-std=c++17"
      export LCG_CPP17="TRUE"
      return 0
    fi
  fi
  return 1
}


#handling of C++14
# enable it only for gcc 5.1 and greater
function cxx14_flag {
  if [[ $COMPILER  == *gcc* ]]; then
    if [ $comp_vers -gt 50 ]; then
      export LCG_CXX_FLAGS="-std=c++14"
      export LCG_CPP14="TRUE"
      return 0
    fi
  elif [[ $COMPILER == *clang* ]]; then
    export LCG_CXX_FLAGS="-std=c++14"
    export LCG_CPP14="TRUE"
    return 0
  fi
  return 1
}

# Export LCG_CXX_FLAGS
cxx17_flag || cxx14_flag || cxx1y_flag || cxx11_flag
