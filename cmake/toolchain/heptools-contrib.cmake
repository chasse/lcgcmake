#---Contribs-----------------------------------------------------------
LCG_external_package(binutils         2.28                                      )
LCG_external_package(gcc              6.2.0                                     )
LCG_external_package(gcc              7.3.0                                     )
LCG_external_package(gcc              8.1.0                                     )
LCG_external_package(clang            6.0.0           gcc=7.3.0                 )
LCG_external_package(clang            7.0.0           gcc=8.1.0                 )
LCG_external_package(cuda             10.2            full=10.2.89_440.33.01    )
LCG_external_package(cuda             10.1            full=10.1.168_418.67      )
LCG_external_package(cuda             11.0RC          full=11.0.1_450.36.06     )
LCG_external_package(cuda             11.1            full=11.1.1_455.32.00     )
