#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

#---Remove unneeded packages---------------------------------------
LCG_remove_package(Geant4)
LCG_remove_package(DD4hep)
LCG_remove_package(acts)
LCG_remove_package(Gaudi)
LCG_remove_package(Garfield++)
LCG_remove_package(COOL)
LCG_remove_package(CORAL)

# Requires a fix in DD4hep
LCG_remove_package(acts)

#---Overwrites and additional packages ----------------------------
LCG_external_package(Python       3.7.6                         ) # TensorRT does not provide binaries for Python 3.8 yet
LCG_external_package(jsonschema   3.0.1                         ) # Version 3.2.0 does not work with Python < 3.8
LCG_external_package(ROOT         HEAD   GIT=http://root.cern.ch/git/root.git        )
LCG_external_package(hepmc3       HEAD   GIT=https://gitlab.cern.ch/hepmc/HepMC3.git )

LCG_external_package(cuda         10.1    full=10.1.168_418.67  )
LCG_external_package(cudnn        7.6.5.32                      )
LCG_external_package(TensorRT     6.0.1.5                       )

LCG_external_package(pycuda       2019.1.2                      )
LCG_external_package(appdirs      1.4.3                         )
LCG_external_package(py_tools     2020.1                        )
LCG_external_package(pybind11     2.5.0                         )
LCG_external_package(pyopencl     2019.1.2                      )
LCG_external_package(mako         1.1.2                         )
LCG_external_package(cupy         7.3.0                         )
LCG_external_package(fastrlock    0.4                           )
