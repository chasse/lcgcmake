set(MCGENPATH  MCGenerators)

LCG_external_package(apfel             3.0.4          ${MCGENPATH}/apfel )

LCG_external_package(sherpa            2.2.11        ${MCGENPATH}/sherpa       author=2.2.11 hepevt=200000)
if(NOT ${LCG_OS}${LCG_OSVERS} MATCHES ubuntu20)  # No MPI support for ubuntu20 native compiler
  LCG_external_package(sherpa-openmpi    2.2.11.openmpi3  ${MCGENPATH}/sherpa    author=2.2.11 hepevt=200000)
endif()

LCG_external_package(openloops         2.1.1          ${MCGENPATH}/openloops)
LCG_external_package(herwig3           7.2.1          ${MCGENPATH}/herwig++ thepeg=2.2.1 hepmc=2)

LCG_external_package(yoda              1.8.3          ${MCGENPATH}/yoda  )
LCG_external_package(rivet             3.1.2          ${MCGENPATH}/rivet hepmc=2)

LCG_external_package(heputils          1.3.2          ${MCGENPATH}/heputils )

LCG_external_package(mcutils           1.3.5          ${MCGENPATH}/mcutils)

LCG_external_package(collier           1.2.4          ${MCGENPATH}/collier)
LCG_external_package(syscalc           1.1.7          ${MCGENPATH}/syscalc)

if(${LCG_COMP} STREQUAL gcc AND (${LCG_COMPVERS} VERSION_LESS 10 OR ${LCG_COMPVERS} VERSION_EQUAL 62))
LCG_external_package(madgraph5amc      2.8.1          ${MCGENPATH}/madgraph5amc)
else()
LCG_external_package(madgraph5amc      2.7.2          ${MCGENPATH}/madgraph5amc)
endif()

LCG_external_package(lhapdf            6.3.0          ${MCGENPATH}/lhapdf       )

if(NOT(${LCG_COMP} STREQUAL clang))
LCG_external_package(powheg-box-v2     r3744.lhcb.rdynamic    ${MCGENPATH}/powheg-box-v2 )
LCG_external_package(agile             1.5.0          ${MCGENPATH}/agile        )
endif()

LCG_external_package(feynhiggs         2.10.2         ${MCGENPATH}/feynhiggs	)
LCG_external_package(chaplin           1.2            ${MCGENPATH}/chaplin      )

LCG_external_package(pythia8           245            ${MCGENPATH}/pythia8 )

LCG_external_package(looptools         2.15           ${MCGENPATH}/looptools)

LCG_external_package(vbfnlo            3.0.0beta5     ${MCGENPATH}/vbfnlo)
LCG_external_package(FORM              4.1            ${MCGENPATH}/FORM)

LCG_external_package(njet              2.0.0          ${MCGENPATH}/njet)
LCG_external_package(qgraf             3.1.4          ${MCGENPATH}/qgraf)
LCG_external_package(gosam_contrib     2.0            ${MCGENPATH}/gosam_contrib)
LCG_external_package(gosam             2.0.4          ${MCGENPATH}/gosam)

LCG_external_package(thepeg            2.2.1          ${MCGENPATH}/thepeg hepmc=2)

LCG_external_package(tauola++          1.1.8          ${MCGENPATH}/tauola++     )

LCG_external_package(pythia6           429.2          ${MCGENPATH}/pythia6    author=6.4.28 hepevt=200000  )

LCG_external_package(photos++          3.64           ${MCGENPATH}/photos++     )

LCG_external_package(evtgen            2.0.0          ${MCGENPATH}/evtgen       tag=R02-00-00 hepmc=2)

LCG_external_package(hijing            1.383bs.2      ${MCGENPATH}/hijing       )

LCG_external_package(starlight         r313            ${MCGENPATH}/starlight   )

LCG_external_package(qd                2.3.13         ${MCGENPATH}/qd          )

LCG_external_package(photos            215.4          ${MCGENPATH}/photos       )
if( NOT ${LCG_COMP} STREQUAL clang ) # Needs CLHEP
LCG_external_package(hepmcanalysis     3.4.14         ${MCGENPATH}/hepmcanalysis  author=00-03-04-14 )
endif()
LCG_external_package(mctester          1.25.0         ${MCGENPATH}/mctester     )

LCG_external_package(herwig            6.521.2        ${MCGENPATH}/herwig       )

LCG_external_package(crmc              1.6.0          ${MCGENPATH}/crmc         )
# LCG_external_package(crmc              1.7.0          ${MCGENPATH}/crmc         )
# ^-- Fails with "multiple definition of `pho_phist_'", no response from authors

LCG_external_package(hydjet            1.8            ${MCGENPATH}/hydjet author=1_8 )
LCG_external_package(tauola            28.121.2       ${MCGENPATH}/tauola       )

LCG_external_package(jimmy             4.31.3         ${MCGENPATH}/jimmy        )
LCG_external_package(hydjet++          2.1            ${MCGENPATH}/hydjet++ author=2_1)
LCG_external_package(alpgen            2.1.4          ${MCGENPATH}/alpgen author=214 )
LCG_external_package(pyquen            1.5.1          ${MCGENPATH}/pyquen author=1_5)
LCG_external_package(baurmc            1.0            ${MCGENPATH}/baurmc       )

LCG_external_package(professor         2.3.2          ${MCGENPATH}/professor    )

LCG_external_package(jhu               5.6.3          ${MCGENPATH}/jhu          )

LCG_external_package(log4cpp           2.9.1          ${MCGENPATH}/log4cpp     )

LCG_external_package(rapidsim          1.4.4          ${MCGENPATH}/rapidsim     )

LCG_external_package(superchic         4.01          ${MCGENPATH}/superchic    )

if(NOT ${LCG_OS} MATCHES ubuntu|mac)
LCG_external_package(hoppet              1.2.0          ${MCGENPATH}/hoppet        )
endif()
if(NOT ${LCG_OS} MATCHES mac)
LCG_external_package(hto4l               2.02           ${MCGENPATH}/hto4l         )
LCG_external_package(prophecy4f          2.0.1          ${MCGENPATH}/prophecy4f    )
endif()

LCG_external_package(ampt                2.26t9b_atlas  ${MCGENPATH}/ampt author=2.26t9b )

LCG_external_package(recola_SM           2.2.2          ${MCGENPATH}/recola_SM      )
LCG_external_package(recola              2.2.0          ${MCGENPATH}/recola         )
if(${LCG_COMP} STREQUAL gcc AND (${LCG_COMPVERS} VERSION_LESS 9 OR ${LCG_COMPVERS} VERSION_EQUAL 62))
LCG_external_package(hjets               1.2-herwig-7.2.1  ${MCGENPATH}/hjets herwig=7.2.1 author=1.2)
# error: ‘const class std::complex<ThePEG::Qty<std::ratio<0>, std::ratio<0>, std::ratio<0> > >’ has no member named ‘__rep’
endif()

LCG_external_package(thep8i              2.0.0          ${MCGENPATH}/thep8i         )
