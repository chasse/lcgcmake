#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

# LCG_external_package(ROOT    v6-22-00-patches   GIT=http://root.cern.ch/git/root.git )
LCG_external_package(ROOT    v6.22.06 )

# if(NOT ${LCG_OS}${LCG_OSVERS} MATCHES fc|ubuntu|mac[0-9]+)
#  LCG_AA_project(CORAL    master     GIT=https://gitlab.cern.ch/lcgcoral/coral.git )
#  LCG_AA_project(COOL     master     GIT=https://gitlab.cern.ch/lcgcool/cool.git   )
# endif()

#---Apple special issues---------------------------------------------
if(APPLE)
  # AppleClang: 'implicit declaration of function 'unwrap2D' is invalid in C99'
  LCG_remove_package(scikitimage)
  # openloops fails building on MacOS (compiler crash)
  LCG_remove_package(openloops)
  LCG_remove_package(sherpa)
  LCG_remove_package(sherpa-openmpi)
  LCG_remove_package(herwig3)
  # Clash between R and ROOT headers (case insensitive) 
  LCG_remove_package(R)
  LCG_remove_package(rpy2)
  # Link error
  LCG_remove_package(unigen)
endif()
