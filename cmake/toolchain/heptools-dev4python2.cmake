#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 2)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

# LCG_external_package(ROOT    v6-22-00-patches  GIT=http://root.cern.ch/git/root.git )
LCG_external_package(ROOT    v6.22.06 )

# Remove COOL/CORAL for time being (tests, at least, failing)
if(${LCG_COMP} MATCHES clang10)
  LCG_remove_package(COOL)
  LCG_remove_package(CORAL)
endif()
LCG_external_package(madgraph5amc      2.8.1.atlas     ${MCGENPATH}/madgraph5amc author=2.8.1)

# Gaudi support python2 only until v34r0
if(${LCG_OS}${LCG_OSVERS} MATCHES centos7|ubuntu18)
  if(((${LCG_COMP} MATCHES gcc) AND (${LCG_COMPVERS} GREATER 7)) OR (${LCG_COMP} MATCHES clang))
    LCG_external_package(Gaudi  v34r0  GIT=https://gitlab.cern.ch/gaudi/Gaudi.git)
  endif()
else()
  LCG_remove_package(Gaudi)
endif()

#---Not supported in Python 2---------------------------------------
LCG_external_package(jsonschema    3.0.1  ) 
LCG_remove_package(jaxlib)
LCG_remove_package(pyhf)
