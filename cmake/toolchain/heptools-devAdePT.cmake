#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Fix some versions----------------------------------------------
LCG_external_package(cuda       11.1      full=11.1.1_455.32.00)
LCG_external_package(VecGeom    v1.1.9                         )

LCG_top_packages(cuda VecGeom veccore Vc alpaka Boost lcgenv CMake ninja doxygen pytest)

#---Remove unneeded packages that create problems------------------
LCG_remove_package(tensorflow)
LCG_remove_package(tensorboard_plugin_wit)
LCG_remove_package(torch)
LCG_remove_package(torchvision)
LCG_remove_package(pyhf)

# We don't need ROOT in dev but without a defined version CMake fails
LCG_external_package(ROOT v6.22.06)
LCG_external_package(cudnn 7.6.5.32)


