#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Define the top level packages for this stack-------------------
LCG_top_packages( backports CMake cx_oracle h5py jupyter_contrib_nbextensions
                  lcgenv logilabcommon numexpr paramiko protobuf pylint pyqt5 
                  pyserial requests scikitlearn seaborn sortedcontainers sqlalchemy xrootd xz)


# We don't need ROOT in devBE but without a defined version CMake fails
LCG_external_package(ROOT v6.22.06)
