#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT     v6.22.06)
LCG_external_package(hepmc3   3.1.1)

#---There are a number of packages not supported with ARM64--------
LCG_remove_package(arrow)
LCG_remove_package(catboost)
LCG_remove_package(cpymad)
LCG_remove_package(llvmlite)
LCG_remove_package(numba)
LCG_remove_package(pyarrow)
LCG_remove_package(pystan)
LCG_remove_package(tensorflow)
LCG_remove_package(tensorboard_plugin_wit)
LCG_remove_package(pyhf)

LCG_remove_package(dcap)
LCG_remove_package(gfal)
LCG_remove_package(srm_ifce)

LCG_remove_package(hydjet)

#---Overwrites of versions ----------(because ARM) ----------------
LCG_external_package(blas      3.8.0.netlib)
LCG_external_package(libtool   2.4.6       )
#---Overwrites of versions ----------(because LHCb tests) ---------
LCG_external_package(xenv      0.0.4       )
LCG_external_package(rangev3   0.5.0       )
LCG_external_package(photos++  3.56         ${MCGENPATH}/photos++ )
LCG_external_package(crmc      1.5.6        ${MCGENPATH}/crmc     )
LCG_external_package(starlight r300         ${MCGENPATH}/starlight)
LCG_external_package(pythia8   240          ${MCGENPATH}/pythia8  )
LCG_external_package(tauola++  1.1.6b.lhcb  ${MCGENPATH}/tauola++ author=1.1.6b )
LCG_external_package(evtgen    1.7.0        ${MCGENPATH}/evtgen   tag=R01-07-00 pythia8=240 tauola++=1.1.6b.lhcb hepmc=2)

#---Define the top level packages for this stack-------------------
LCG_top_packages(Geant4 ROOT CMake evtgen pythia8 lcgenv rangev3 cppgsl CppUnit QMtest nose gperftools six networkx ninja xenv
                 RELAX eigen vectorclass lxml rivet fastjet pythia6 crmc starlight AIDA libgit2 ipython fmt
                 COOL CORAL pandas heputils mcutils psutil pygraphviz sqlalchemy requests)
