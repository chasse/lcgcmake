#---List of externals
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT         HEAD   GIT=http://root.cern.ch/git/root.git        )
LCG_external_package(hepmc3       HEAD   GIT=https://gitlab.cern.ch/hepmc/HepMC3.git )
LCG_external_package(DD4hep        master   GIT=https://github.com/AIDASoft/DD4hep.git   )
if(NOT ${LCG_OS}${LCG_OSVERS} MATCHES fc|ubuntu|mac[0-9]+)
  LCG_AA_project(CORAL    master     GIT=https://gitlab.cern.ch/lcgcoral/coral.git )
  LCG_AA_project(COOL     master     GIT=https://gitlab.cern.ch/lcgcool/cool.git   )
endif()
if(${LCG_OS}${LCG_OSVERS} MATCHES centos7|ubuntu18)
  if(((${LCG_COMP} MATCHES gcc) AND (${LCG_COMPVERS} GREATER 7)) OR (${LCG_COMP} MATCHES clang))
    LCG_external_package(Gaudi master   GIT=https://gitlab.cern.ch/gaudi/Gaudi.git)
  endif()
endif()

if(APPLE)
  # AppleClang: 'implicit declaration of function 'unwrap2D' is invalid in C99'
  LCG_remove_package(scikitimage)
  # openloops fails building on MacOS (compiler crash)
  LCG_remove_package(openloops)
  LCG_remove_package(sherpa)
  LCG_remove_package(sherpa-openmpi)
  LCG_remove_package(herwig3)
  # Clash between R and ROOT headers (case insensitive) 
  LCG_remove_package(R)
  LCG_remove_package(rpy2)
  # Link error
  LCG_remove_package(unigen)
endif()

LCG_external_package(pythia8           303            ${MCGENPATH}/pythia8 )
LCG_external_package(madgraph5amc      2.8.0.atlas    ${MCGENPATH}/madgraph5amc author=2.8.0)

# Requires a fix in DD4hep
LCG_remove_package(acts)
