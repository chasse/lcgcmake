#---List of externals
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT         HEAD   GIT=http://root.cern.ch/git/root.git        )
LCG_external_package(hepmc3       HEAD   GIT=https://gitlab.cern.ch/hepmc/HepMC3.git )
LCG_remove_package(Gaudi)

LCG_remove_package(Python)
LCG_external_package(Python 3.8.6)

# root_numpy is depricated in favour of uproot; doesn't work with 3.8
LCG_remove_package(root_numpy)
