# TODO check -O compile options
# for many packages -O0 is used (

LCGPackage_Add(
  FORM
  URL ${gen_url}/FORM-v<NATIVE_VERSION>.tar.gz
  CONFIGURE_COMMAND ./configure --prefix=<INSTALL_DIR> CC=${CMAKE_C_COMPILER} CXX=${CMAKE_CXX_COMPILER}
  BUILD_IN_SOURCE 1
)

# TODO cannot link feynhiggs
LCGPackage_Add(
  vbfnlo
  URL ${gen_url}/vbfnlo-<NATIVE_VERSION>.tgz
  CONFIGURE_COMMAND ${CMAKE_COMMAND} -E touch <SOURCE_DIR>/utilities/VBFNLOConfig.h.in
            COMMAND ./configure --prefix=<INSTALL_DIR>
                    --with-hepmc=${HepMC_home}
                    --with-gsl=${GSL_home}
                    --with-LHAPDF=${lhapdf_home}
                    --with-LOOPTOOLS=${looptools_home} "FCFLAGS=-std=legacy"
#                    --with-FEYNHIGGS=${feynhiggs_home}
  BUILD_IN_SOURCE 1
  DEPENDS HepMC GSL lhapdf looptools feynhiggs
)

# is vc package needed?
LCGPackage_Add(
  njet
  URL ${gen_url}/njet-<NATIVE_VERSION>.tar.gz
  CONFIGURE_COMMAND ./configure --prefix=<INSTALL_DIR>
                    --disable-autoflags
                    --with-qd=${qd_home} "FFLAGS=-ffixed-line-length-none -std=legacy" FC=${CMAKE_Fortran_COMPILER} CC=${CMAKE_C_COMPILER} F77=${CMAKE_Fortran_COMPILER}
  BUILD_IN_SOURCE 1
  DEPENDS qd
)

# TODO clean from waste
LCGPackage_Add(
  qgraf
  URL ${gen_url}/qgraf-3.1.4.tgz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND ${CMAKE_Fortran_COMPILER} -o qgraf qgraf-3.1.4.f
  INSTALL_COMMAND ${CMAKE_COMMAND} -E make_directory <INSTALL_DIR>/bin
          COMMAND ${CMAKE_COMMAND} -DSRC=<SOURCE_DIR> -DDST=<INSTALL_DIR>/bin -P ${CMAKE_SOURCE_DIR}/cmake/scripts/copy.cmake
  BUILD_IN_SOURCE 1
)

LCGPackage_Add(
  gosam
  URL ${gen_url}/gosam-<NATIVE_VERSION>.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND ${PYTHON} setup.py build
  INSTALL_COMMAND ${PYTHON} setup.py install --prefix=<INSTALL_DIR>
          COMMAND chmod -R go+r <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS gosam_contrib qgraf FORM Python
)


#--Herwig3 ----------------------
set(thepeg3_v_home ${thepeg_home})
set(lhapdf_v_home ${lhapdf_home})
set(openloops_v_home ${openloops_home})
set(madgraph_v_home ${madgraph5amc_home})
set(gosam_v_home ${gosam_home})
LCGPackage_Add(
    herwig3
    URL ${gen_url}/Herwig-<NATIVE_VERSION>.tar.bz2
    IF LCG_ARCH STREQUAL "aarch64" THEN PATCH_COMMAND patch -p0 < ${CMAKE_CURRENT_SOURCE_DIR}/patches/herwig3-<NATIVE_VERSION>-aarch64.patch ENDIF
    CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory <INSTALL_DIR>/tmppdfsets
    COMMAND ${lhapdf_v_home}/bin/lhapdf --pdfdir=<INSTALL_DIR>/tmppdfsets --source=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current --listdir=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current install MMHT2014lo68cl MMHT2014nlo68cl IF <VERSION> VERSION_GREATER 7.1.9 THEN CT14lo CT14nlo ENDIF
    COMMAND autoreconf --force --install
    COMMAND ./configure --prefix=<INSTALL_DIR>
                                  --with-gsl=${GSL_home}
                                  --with-thepeg=${thepeg3_v_home}
                                  --with-thepeg-headers=${thepeg3_v_home}/include
                                  --with-fastjet=${fastjet_home}
                                  --with-boost=${Boost_home}
                                  --with-madgraph=${madgraph_v_home}
                                  --with-openloops=${openloops_v_home}
                                  --with-gosam-contrib=${gosam_contrib_home}
                                  --with-gosam=${gosam_v_home}
                                  --with-njet=${njet_home}
                                  --with-vbfnlo=${vbfnlo_home}
#                                 ${library_path}=${lhapdf_v_home}/lib:${GSL_home}/lib:$ENV{${library_path}}
                                 "CXXFLAGS=${CMAKE_CXX_FLAGS}"
                                 "FCFLAGS=${CMAKE_Fortran_FLAGS} -std=legacy"
   BUILD_COMMAND ${EXEC} CXXFLAGS=${CMAKE_CXX_FLAGS} "FFLAGS=${CMAKE_Fortran_FLAGS} -std=legacy"
                 ${MAKE} all 
#${library_path}=${lhapdf_v_home}/lib:${thepeg3_v_home}/lib/ThePEG:${GSL_home}/lib:${Boost_home}/lib:${fastjet_home}/lib:$ENV{${library_path}} LIBRARY_PATH=${fastjet_home}/lib

#    BUILD_COMMAND ${MAKE} all ${library_path}=${lhapdf_v_home}/lib:${thepeg3_v_home}/lib/ThePEG:${GSL_home}/lib:${Boost_home}/lib:${fastjet_home}/lib:$ENV{${library_path}} LIBRARY_PATH=${fastjet_home}/lib
          IF ((<VERSION> VERSION_GREATER 7.0.3) AND (<VERSION> VERSION_LESS 7.1.9)) THEN
            COMMAND ${CMAKE_COMMAND} -E chdir Contrib/FxFx ${EXEC}
              PATH=${rivet_home}/bin:$ENV{PATH}
              HERWIGINCLUDE=-I../../include
              BOOSTINCLUDE=-I${Boost_home_include}
              ${MAKE}
          ENDIF
          IF (<VERSION> VERSION_GREATER 7.1.9) THEN
            COMMAND ${CMAKE_COMMAND} -E chdir MatrixElement/FxFx ${EXEC}
              PATH=${rivet_home}/bin:$ENV{PATH}
              HERWIGINCLUDE=-I../../include
              BOOSTINCLUDE=-I${Boost_home_include}
              ${MAKE}
          ENDIF

    INSTALL_COMMAND IF <herwig3_<VERSION>_thepeg> VERSION_LESS 2.2.0 OR <thepeg_<herwig3_<VERSION>_thepeg>_hepmc> EQUAL 2 THEN 
      ${MAKE} install ${library_path}=${lhapdf_v_home}/lib:${thepeg3_v_home}/lib/ThePEG:${GSL_home}/lib:${Boost_home}/lib:${HepMC_home}/lib:${fastjet_home}/lib:$ENV{${library_path}} LIBRARY_PATH=${fastjet_home}/lib:${thepeg3_v_home}/lib/ThePEG:${lhapdf_v_home}/lib LHAPDF_DATA_PATH=<INSTALL_DIR>/tmppdfsets
    ELSE
      ${MAKE} install LHAPDF_DATA_PATH=<INSTALL_DIR>/tmppdfsets
      #${library_path}=${lhapdf_v_home}/lib:${thepeg3_v_home}/lib/ThePEG:${GSL_home}/lib:${Boost_home}/lib:${hepmc3_home}/lib:${fastjet_home}/lib:$ENV{${library_path}} LIBRARY_PATH=${fastjet_home}/lib:${thepeg3_v_home}/lib/ThePEG:${lhapdf_v_home}/lib LHAPDF_DATA_PATH=<INSTALL_DIR>/tmppdfsets
    ENDIF
            COMMAND ${CMAKE_COMMAND} -E remove -f <INSTALL_DIR>/tmppdfsets
          IF ((<VERSION> VERSION_GREATER 7.0.3) AND (<VERSION> VERSION_LESS 7.1.9)) THEN
            COMMAND ${CMAKE_COMMAND} -E chdir Contrib/FxFx ${EXEC} HERWIGINSTALL=<INSTALL_DIR> make install
          ENDIF
          IF <VERSION> VERSION_GREATER 7.1.9 THEN
            COMMAND ${CMAKE_COMMAND} -E chdir MatrixElement/FxFx ${EXEC} HERWIGINSTALL=<INSTALL_DIR> make install
          ENDIF
    BUILD_IN_SOURCE 1
    DEPENDS lhapdf Boost Python GSL thepeg fastjet vbfnlo openloops madgraph5amc njet gosam automake
)

foreach(v ${herwig3_native_version})
set (vv "")
foreach(herwigtest LHC-Matchbox-MadGraph-MadGraph LHC-Matchbox-MadGraph-OpenLoops LHC-Matchbox-MadGraph-GoSam)
LCG_add_test(herwig3-${v}.${herwigtest}
                          TEST_COMMAND $ENV{SHELL} -c "${lcgenv_home}/lcgenv -p ${CMAKE_INSTALL_PREFIX} ${LCG_platform} herwig3 ${v} > h3-${v}.env
                            source h3-${v}.env
                            rm -rf Herwig-scratch
                            ${herwig3-${v}_home}/bin/Herwig build ${CMAKE_SOURCE_DIR}/generators/Herwig3/${herwigtest}.in
                            ${herwig3-${v}_home}/bin/Herwig integrate LHC-Matchbox.run"
                          POST_COMMAND cat Herwig-scratch/Build/MadGraphAmplitudes/MG.log
                          ENVIRONMENT LHAPDF_DATA_PATH=${herwig3-${v}_home}/tmppdfsets
                          ${vv}
                          )
set (vv DEPENDS herwig3-${v}.${herwigtest})
endforeach()
endforeach()

#---HJets------------------------------------------------------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  hjets
  URL ${gen_url}/HJets-<hjets_<NATIVE_VERSION>_author>.tar.bz2
  CONFIGURE_COMMAND ./configure --prefix=<INSTALL_DIR> --with-herwig=${herwig3_home}
  BUILD_IN_SOURCE 1
  DEPENDS herwig3
)

#---TheP8I-----------------------------------------------------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  thep8i
  URL ${gen_url}/TheP8I-<NATIVE_VERSION>.tar.gz
  ENVIRONMENT THEPEGPATH=${thepeg_home} LD_LIBRARY_PATH=${thepeg_home}/lib/ThePEG:\$ENV{LD_LIBRARY_PATH}
  CONFIGURE_COMMAND autoreconf -ivf
            COMMAND ./configure --prefix=<INSTALL_DIR> --libdir=<INSTALL_DIR>/lib64 --with-pythia8=${pythia8_home} --with-gsl=${GSL_home}
  # BUILD_COMMAND ${MAKE}
  # INSTALL_COMMAND ${MAKE} install
  BUILD_IN_SOURCE 1
  DEPENDS pythia8 thepeg GSL
)
