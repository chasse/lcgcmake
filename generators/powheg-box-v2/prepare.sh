#!/usr/bin/env bash
# This script downloads sources necessary to build POWHEG library and requires the following arguments.
# 1: POWHEG-BOX version.
# 2: SVN revision number.
# 3: library to build.
# The following environment variables should be set at build time
# FCOMP: Fortran compiler.
# CCOMP: C++ compiler.
# LHAPDF: LHAPDF directory.
# FASTJET: FASTJET directory.

# Based on a script provided by LHCb: 
# https://gitlab.cern.ch/lhcb/Gauss/-/blob/7496abc83308dd47af6262f99ccb48582ef4caf7/Gen/LbPowheg/src/plugin_helper/build.sh

# Set the input.
shopt -s expand_aliases
FTC='$(FCOMP)'
CXX='$(CCOMP)'
LPI='$(LHAPDF)'/include
FJI='$(FASTJET)'/include
VER=$1
REV=$2
LIB=$3
TOP=$PWD

# Check out the source.
SVN="svn export --revision $REV --username anonymous --password anonymous"
SVN+=" --force --no-auth-cache --non-interactive"
REP="svn://powhegbox.mib.infn.it/trunk"
if [ $VER == 2 ]; then COM=POWHEG-BOX-V2; PRO=User-Processes-V2
else COM=POWHEG-BOX-NoUserProcesses; PRO=POWHEG-BOX; fi
if [ ! -d POWHEG$VER ]; then $SVN $REP/$COM POWHEG$VER; fi
if [ -z $LIB ]; then exit; fi
cd POWHEG$VER
if [ ! -d $LIB ]; then $SVN $REP/$PRO/$LIB; fi
cd $LIB

# LHAPDF.
LHAPDF_INC=$(dirname $LPI)
LHAPDF_LIB="-Wl,-rpath,${LHAPDF_INC%lib/}/lib -L${LHAPDF_INC%lib/}/lib"
LHAPDF_LIB+=" -lLHAPDF -lstdc++"
LHAPDF_CFG="${LHAPDF_INC%lib/}/bin/lhapdf-config"

# FASTJET.
FASTJET_CFG="$FJI/../bin/fastjet-config"

# QCDLoops.
if [ $LIB = "ST_wtch_DR" ]; then QCD="QCDLoop-1.96"
elif [ $LIB = "ST_wtch_DS" ]; then QCD="QCDLoop-1.9"
else QCD=; fi

# Compiler flags.
FTC+=" -fno-automatic -rdynamic -fPIE -fPIC -pie -ffixed-line-length-none"
FTC+=" -std=legacy"
CXX+=" -fPIC"

# Compiler variables to replace.
CMPS="FC F77 F90 FORTCOMP F77local"
if [ $LIB = "gg_H" ]; then CMPS+=" FF"; fi

# Include paths.
if [ $LIB = "ttb_NLO_dec" ]; then FTC+=" -I./MCFM/src/Inc -I./interfaceinclude"
elif [ $LIB = "VBF_W-Z" ]; then FTC+=" -I../include"; fi

# Check for broken symlinks and pull missing source.
for OBJ in $(find ./ -type l ! -exec test -e {} \; -print); do
    SYM=$(readlink $OBJ)
    if [ "${SYM:0:3}" != "../" ]; then continue; fi
    $SVN $REP/$PRO/${SYM:3} $OBJ
done

# Handle QCDLoops requirement.
if [ ! -z $QCD ] && [ ! -d $QCD ]; then
    wget https://qcdloop.fnal.gov/$QCD.tar.gz; tar -xzf $QCD.tar.gz; rm $QCD.tar.gz; fi

# Fix the generic build system.
if [ -f lhapdf6if.f ]; then cp ../lhapdf6if.f lhapdf6if.f; fi
for FILE in $(find . -name "[Mm]akefile") $(find . -name "[Mm]akefile.g*"); do
    if [ ! -f $FILE ]; then continue; fi
    if [ ! -f $FILE.bak ]; then cp $FILE $FILE.bak;
    else cp $FILE.bak $FILE; fi
    sed -i "s~^ *COMPILER=.*~COMPILER=gfortran~g" $FILE
    sed -i "s~^ *PDF *=.*~PDF=lhapdf~g" $FILE
    sed -i "s~^ *DEBUG *=.*~DEBUG=~g" $FILE
    sed -i "s~^ *CXX *=.*~CXX=$CXX~g" $FILE
    sed -i "s~-\$(FC)~-\$(COMPILER)~g" $FILE
    sed -i "s~/\$(FC)/~/\$(COMPILER)/~g" $FILE
    sed -i "s~\$(MAKE) *FC=\$(COMPILER)~make~g" $FILE
    sed -i "s~make *FC=\$(COMPILER)~make~g" $FILE
    sed -i "s~make *FC=\$(FC)~make~g" $FILE
    sed -i "s~\$(F77)~$FTC~g" $FILE
    sed -i "s~\$(CC)~$CXX~g" $FILE
    for CMP in $CMPS; do
	sed -i "s~^ *$CMP *=.*~$CMP=$FTC"$"\nCXX=$CXX~g" $FILE
    done
    sed -i "s~fastjet-config~$FASTJET_CFG~g" $FILE
    sed -i "s~^ *LIBSLHAPDF *=.*~LIBSLHAPDF=$LHAPDF_LIB~g" $FILE
    sed -i "s~^ *LHAPDF_CONFIG *=.*~LHAPDF_CONFIG=$LHAPDF_CFG~g" $FILE
    sed -i "s~^ *FASTJET_CONFIG *=.*~FASTJET_CONFIG=$FASTJET_CFG~g" $FILE
    sed -i "s~\$(shell ../svnversion/svnversion.sh>/dev/null)~~g" $FILE
    sed -i "s~pwhg_main.o: svn.version~~g" $FILE
    sed -i "s~lhefwrite.o: svn.version~~g" $FILE
    if [ $LIB = "Wgamma" ]; then
	sed -i "s~^ *ANALYSIS *=.*~ANALYSIS=dummy~g" $FILE
    fi
    if [ ! -z $QCD ]; then
        sed -i "\~^LIBS+=-lz~a \ \nQCDLOOP:\n\tcd ${QCD};\ \$(MAKE)\ MAKELEVEL=0" $FILE
        sed -i "s~pwhg_main:\(.*\)~pwhg_main:\ \$(QCDLOOP)\ \1~g" $FILE
    fi
    sed -i '/%.o: %.f90 $(INCLUDE) | $(OBJDIR)/ c\
%.o: ./GoSamlib/%.f90 $(INCLUDE) | $(OBJDIR)\
\	$(FF) -fautomatic -std=legacy -c -o $(OBJ)/$@ $<\
\
%.o: %.f90 $(INCLUDE) | $(OBJDIR)' $FILE
done

# Fix PDF initialization bug for ST_tch_4f.
FILES=$(grep -Ril --include=*.{f,F,f90,F90} "alphasPDF(" ./)
for FILE in $FILES; do
    if [ ! -f $FILE ]; then continue; fi
    if [ ! -f $FILE.bak ]; then cp $FILE $FILE.bak;
    else cp $FILE.bak $FILE; fi
    sed -i "s~alphasPDF~alphasfrompdf~gi" $FILE
done

# Fix multiple issues with Wbb_nodec.
if [ $LIB = "Wbb_nodec" ]; then
    cp ../pwhg_analysis_driver.f ./
    OBJ="lhefread.o pwhg_io_interface.o rwl_weightlists.o"
    OBJ+=" rwl_setup_param_weights.o"
    sed -i "s~\$(STATIC)  -o \$@~\$(STATIC)  -o \$@ -lz~g" Makefile
    sed -i "s~\(\$(PDFPACK) \$(USER) \$(FPEOBJ)\)~\1 $OBJ~g" Makefile
fi

# Fix compiler specification for Wp_Wp_J_J.
if [ $LIB = "Wp_Wp_J_J" ]; then
    sed -i "s~F90 ; make~F90 ; make FC=\$(COMPILER)~g" Makefile
fi

# Fix headers for VBF_W-Z.
if [ $LIB = "VBF_W-Z" ]; then
    sed -i "s~../pwhg_book.h~pwhg_book.h~g" pwhg_analysis.f
fi

# Fix headers for dislepton-jet.
if [ $LIB = "dislepton-jet" ]; then
    rm Madlib/coupl.inc && cp MODEL/coupl.inc Madlib/coupl.inc
fi
