//---Dynamic Global Variables------------------------------------------------------------------------------------------- 
def PLATFORM = 'UNKNOWN'
def barePLATFORM = 'UNKNOWN'
def urlPLATFORM = 'UNKNOWN'
def CTEST_TAG = ''
def BUILDHOSTNAME = ''
def CVMFS_REVISION = '0'

//----------------------------------------------------------------------------------------------------------------------
//---Functions for Steps------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

def initScript() {
  def BTYPE = [Release: 'opt', Debug: 'dbg']
  currentBuild.displayName = "#${BUILD_NUMBER}" + ' ' + params.LCG_VERSION + '-' + params.LABEL + '-' + params.COMPILER + '-' + BTYPE[params.BUILDTYPE]
  return 'done'
}

def buildPackages() {
  catchError(buildResult: 'UNSTABLE', stageResult: 'FAILURE') {
    sh label: 'build_and_test', script: """
      source lcgcmake/jenkins/jk-setup.sh $BUILDTYPE $COMPILER
      env | sort | sed 's/:/:?     /g' | tr '?' '\n'
      ctest -VV -DCTEST_LABELS=Release  -S lcgcmake/jenkins/lcgcmake-build.cmake
    """
  }
  //---Update a number of variables after the build step-----------------------------------------------------------------
  PLATFORM = sh(returnStdout: true, script: 'lcgcmake/jenkins/getPlatform.py').trim()
  barePLATFORM = ([PLATFORM.split('-')[0].split('\\+')[0]]+PLATFORM.split('-')[1..3]).join('-').replace('gcc7','gcc8').replace('dbg','opt')
  urlPLATFORM = PLATFORM.replaceAll('\\+','%2B')
  CTEST_TAG = sh(returnStdout: true, script: 'cat build/Testing/TAG').trim()
  BUILDHOSTNAME = sh(returnStdout: true, script: '[ ${container} ] &&  echo ${NODE_NAME}-${container} ||  hostname').trim()
}

def copyToEOS() {
  sh label: 'copy_to_eos', script: """
    export PLATFORM=${PLATFORM}
    if [ -d /cvmfs/sft.cern.ch/lcg/releases/LCG_98python3/xrootd/4.12.3/${barePLATFORM} ]; then
      set +x; source /cvmfs/sft.cern.ch/lcg/releases/LCG_98python3/xrootd/4.12.3/${barePLATFORM}/xrootd-env.sh; set -x
    fi
    lcgcmake/jenkins/copytoEOS.sh
    if [[ $BUILDMODE == nightly ]]; then
      lcgcmake/jenkins/copytoLatest.sh
    fi
  """
}

def cvmfsVolume() { 
  return params.BUILDMODE == 'nightly' ? 'sft-nightlies.cern.ch' : 'sft.cern.ch'
}

def testLABEL(String label, String version) {
  if (version =~ 'cuda') {
    return 'cuda10' 
  }
  else if( label =~ 'mac') {
    return label + '-cvmfs'
  }
  else {
    return label
  }
}

def preinstallMacOS() {
  if (params.BUILDMODE == 'nightly') {
    def weekday = Calendar.getInstance().format("EEE")
    def prefix = '/Users/Shared/cvmfs/sft-nightlies.cern.ch/lcg/nightlies'
    def views = '/Users/Shared/cvmfs/sft-nightlies.cern.ch/lcg/views'
    sh  label:  'preinstall-macos', script: """
      mkdir -p ${prefix}/${LCG_VERSION}/${weekday}
      rm -rf ${prefix}/${LCG_VERSION}/${weekday}/*
      mkdir -p ${views}/${LCG_VERSION}
      mkdir -p preinstall
      rm -rf preinstall/*
      cd preinstall
      ../lcgcmake/bin/lcgcmake reset
      ../lcgcmake/bin/lcgcmake config --version ${LCG_VERSION} \
                                      --prefix ${prefix} \
                                      --nightly
      ../lcgcmake/bin/lcgcmake install ${TARGET}
    """
  }
  else if (params.BUILDMODE == 'release') {
    def prefix = '/Users/Shared/cvmfs/sft.cern.ch/lcg/releases'
    def views = '/Users/Shared/cvmfs/sft.cern.ch/lcg/views'
    sh  label:  'preinstall-macos', script: """
      unset RELEASE_MODE
      mkdir -p ${prefix}/LCG_${LCG_VERSION}
      rm -rf ${prefix}/LCG_${LCG_VERSION}/*
      mkdir -p ${views}/LCG_${LCG_VERSION}
      mkdir -p preinstall
      rm -rf preinstall/*
      cd preinstall
      ../lcgcmake/bin/lcgcmake reset
      ../lcgcmake/bin/lcgcmake config --version LCG_${LCG_VERSION} \
                                      --prefix ${prefix} \
                                      -o LCG_ADDITIONAL_REPOS='' \
                                      --toolchain ../lcgcmake/cmake/toolchain/heptools-${LCG_VERSION}.cmake
      ../lcgcmake/bin/lcgcmake install ${TARGET}
    """
  }
}

def installCVMFS() {
  if (params.LABEL =~ 'mac') {
    sh label: 'cvmfs_install', script: """
      export PLATFORM=${PLATFORM}
      export BUILDHOSTNAME=${BUILDHOSTNAME}
      lcgcmake/jenkins/cvmfs_install_mac.sh
    """
  } else {
    sh label: 'cvmfs_install', script: """
      export PLATFORM=${PLATFORM}
      lcgcmake/jenkins/cvmfs_install.sh
    """
  }
  //---Update variable after the install step---------------------------------------------------------------------------
  def volume = [release: 'sft.cern.ch', nightly: 'sft-nightlies.cern.ch']
  CVMFS_REVISION = sh(returnStdout: true, script: "attr -qg revision /var/spool/cvmfs/${cvmfsVolume()}/rdonly/")
}

def waitForCVMFS() {
  sh  label:  'wait-for-cvmfs', script: """
    lcgcmake/jenkins/wait_for_cvmfs.sh ${cvmfsVolume()} ${CVMFS_REVISION}
  """
}

def cloneRepository(String repo, String project = 'sft/lcgtests', String branch = 'master') {
  dir(repo) {
    git branch: branch,
        credentialsId: '7c3a9bea-6657-46fc-901d-bef5c7e0061c',
        url: "https://gitlab.cern.ch/${project}/${repo}.git"
  }
}

def testWithViews() {
  sh  label: 'test-with-views', script: """
    export PLATFORM=${PLATFORM}
    export BUILDHOSTNAME=${BUILDHOSTNAME}
    rm -rf test_build
    mkdir -p test_build/Testing
    echo "${CTEST_TAG}" > test_build/Testing/TAG
    source /cvmfs/sft.cern.ch/lcg/views/$LCG_VERSION/latest/$PLATFORM/setup.sh
    if [[ $PLATFORM == *slc6* ]]; then export KERAS_BACKEND=theano; fi
    ctest -V -DCTEST_LABELS=".*" -S lcgcmake/jenkins/lcgtest-test.cmake
  """
}

def testShell() {
  sh  label: 'test-shell',  script: """
    export PLATFORM=${PLATFORM}
    export BUILDHOSTNAME=${BUILDHOSTNAME}
    if [ \$(uname) != 'Darwin' ]; then
      export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.17.1/Linux-`uname -m`/bin:\${PATH}
    fi
    rm -rf csh_build
    mkdir -p csh_build/Testing
    echo "${CTEST_TAG}" > csh_build/Testing/TAG
    export VIEW=/cvmfs/sft.cern.ch/lcg/views/$LCG_VERSION/latest/$PLATFORM
    ctest -V -DCTEST_LABELS=".*" -S lcgcmake/jenkins/lcgtest-csh.cmake
  """
}

def testPythonImport() {
  sh  label: 'test-pythonimport', script: """
    set +e
    export PLATFORM=${PLATFORM}
    export BUILDHOSTNAME=${BUILDHOSTNAME}
    export MODE=`echo "${CTEST_TAG}" |  sed '2q;d'`
    export CTEST_TIMESTAMP=`echo "${CTEST_TAG}" | head -1`
    cd lcg-import-test
    python python_test.py ${LCG_VERSION} ${PLATFORM}
    exit_code=\$?
    testxml=\$WORKSPACE/Test.xml
    checksum=\$(md5sum \$testxml | cut -d ' ' -f 1)
    curl -s --upload-file \$testxml "http://cdash.cern.ch/submit.php?project=LCGSoft&FileName=${BUILDHOSTNAME}___${LCG_VERSION}-${urlPLATFORM}___\${CTEST_TIMESTAMP}-\${MODE}___XML___Test.xml&build=${LCG_VERSION}-${urlPLATFORM}&site=${BUILDHOSTNAME}&stamp=\${CTEST_TIMESTAMP}-\${MODE}&MD5=\${checksum}"
    exit \$exit_code
  """
}

def createRPMS() {
  sh  label: 'create-rpms', script: """
    XRDROOT=/cvmfs/sft.cern.ch/lcg/releases/LCG_98python3/xrootd/4.12.3/x86_64-centos7-gcc8-opt
    if [ -d \$XRDROOT ]; then
       source /cvmfs/sft.cern.ch/lcg/contrib/gcc/8/x86_64-centos7/setup.sh
       export LD_LIBRARY_PATH=\$XRDROOT/lib64:\$LD_LIBRARY_PATH
       export PATH=\$XRDROOT/bin:\$PATH
    fi
    cd ${WORKSPACE}/install
    ${WORKSPACE}/lcgcmake/jenkins/package_release.py ${WORKSPACE} ${LCG_VERSION} ${PLATFORM} ${TARGET} ${BUILDMODE} ${COMPILER}
  """
}

def publishRPMS() {
  sh  label: 'publish-rpms', script: """
    export PYTHONUNBUFFERED=TRUE
    export EOS_MGM_URL=root://eosproject-l.cern.ch/
    if [ "$RPM_REPOSITORY" == "" ]; then
      if [[ $LCG_VERSION =~ ^([0-9]+).* ]]; then
        REPO=/eos/project/l/lcg/www/lcgpackages/lcg/repo/\${LABEL: -1}/LCG_\${BASH_REMATCH[1]}/
      else
        echo "No RPM_REPOSITORY provided and cannot deduce from version $LCG_VERSION"
        exit 1
      fi
    else
      REPO=$RPM_REPOSITORY
    fi
    kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab
    eosfusebind
    if [ ${RPM_DRYRUN} == 'yes' ]; then
      lcgcmake/jenkins/complete_rpms_actions.py ${LCG_VERSION} \$REPO \$(echo ${PLATFORM} | sed -e 's/-/_/g') --dryrun
    else
      lcgcmake/jenkins/complete_rpms_actions.py ${LCG_VERSION} \$REPO \$(echo ${PLATFORM} | sed -e 's/-/_/g')
      createrepo --update --workers=20 \$REPO
    fi
  """
}

def testRPMS() {
  sh  label: 'test-rpms', script: """
    PLATFORM=$PLATFORM
    LCG_RELEASE=$LCG_VERSION
    LCG_RELEASE_BASE=\$(echo \$LCG_RELEASE | cut -d '_' -f 1 | sed -e 's/python3//g' | sed -e 's/[^0-9]//g')
    export YUM0="\${LABEL: -1}"
    PLATFORMU=\$(echo \$PLATFORM | sed -e 's/-/_/g')
    TARGETRPM=LCG_\${LCG_RELEASE}_\${PLATFORMU}
    curl https://lcgpackages.web.cern.ch/lcg/etc/yum.repos.d/lcg\${YUM0}.repo > /etc/yum.repos.d/lcg.repo
    cat /etc/yum.repos.d/lcg.repo
    # rpm --rebuilddb
    # yum update -y
    yum install -y \${TARGETRPM}
  """
}

def publishINFO() {
  sh  label: 'publish-info', script: """
    export TYPE=$BUILDMODE
    export LCG_RELEASE=$LCG_VERSION
    export DESCRIPTION='Publication of release \${LCG_RELEASE}'
    export WORKSPACE=$WORKSPACE/lcginfo
    lcginfo/db_publish.sh
  """
}

return this;