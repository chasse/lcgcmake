#!/bin/bash

# Accepting following parameters:
#  $1: buildtype (Debug,Release)
#  $2: compiler version
#  $3: slotname

THIS=$(dirname ${BASH_SOURCE[0]})
ARCH=$(uname -m)

if [ $# -ge 2 ]; then
  BUILDTYPE=$1; shift
  COMPILER=$1; shift
else
  echo "$0: expecting 2 arguments: [buildtype] [compiler]"
  return
fi

# A few default parameters of the build--------------------------------------------
export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8

# Set up the build variables-------------------------------------------------------
export PDFSETS=minimal
export BUILDTYPE
export COMPILER
export PLATFORM=`$THIS/getPlatform.py`
export TODAY=$(date | awk '{print $1}')
if [ -z "$TARGET" ]; then export TARGET=all; fi
export EOS_MGM_URL="root://eosproject-l.cern.ch"

# setup cmake and AFS token for slc6 nodes ----------------------------------------

if [[ $PLATFORM == *slc6* ]]; then
    LABEL_COMPILER=slc6
fi
if [[ $PLATFORM == *centos7* ]]; then
    LABEL_COMPILER=centos7
fi
if [[ $PLATFORM == *centos8* ]]; then
    LABEL_COMPILER=centos8
fi

# setup CMake -----(the default version is often very old)-------------------------
if [[ $PLATFORM == *slc6* || $PLATFORM == *cc7* || $PLATFORM == *centos* || $PLATFORM == *ubuntu* || $PLATFORM == *fedora* ]]; then
    if [[ ${ARCH} == *i686* ]]; then
        # i686 special case: the newer cmake is a link to x86_64, might cause problems!!
        export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.3.2/Linux-i686/bin:${PATH}
    else
        export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.17.1/Linux-${ARCH}/bin:${PATH}
    fi
fi

#if [[ $PLATFORM == *slc6* || $PLATFORM == *cc7* || $PLATFORM == *centos7* || $PLATFORM == *ubuntu* || $PLATFORM == *fedora* ]]; then
#    kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab
#elif [[ $PLATFORM == *mac* ]]; then
#    kinit -V -k -t /build/conf/sftnight.keytab
#fi

# setup compiler-------------------------------------------------------------------
if [[ $COMPILER == *gcc8* && $PLATFORM == *ubuntu* ]]; then
  # Setting up gcc8 from the official installation on Ubuntu 18.04
  export FC=`command -v gfortran-8`
  export CXX=`command -v g++-8`
  export CC=`command -v gcc-8`
  export ExtraCMakeOptions="${ExtraCMakeOptions}"
elif [[ $COMPILER == *gcc* ]]; then
  gcc47version=4.7
  gcc48version=4.8
  gcc49version=4.9
  gcc51version=5.1
  gcc52version=5.2
  gcc52version=5.2
  gcc53version=5.3
  gcc61version=6.1
  gcc62version=6.2
  gcc63version=6.3.0
  gcc7version=7
  gcc8version=8
  gcc9version=9
  gcc10version=10
  gcc61abi4version=6.1abi4
  gcc62abi4version=6.2abi4
  gcc62binutilsversion=6.2binutils
  gcc7binutilsversion=7binutils
  gcc8binutilsversion=8binutils
  gcc8testingversion=8testing
  gcc9binutilsversion=9binutils
  gcc10binutilsversion=10binutils
  gcc9testingversion=9testing
  COMPILERversion=${COMPILER}version
  source /cvmfs/sft.cern.ch/lcg/contrib/gcc/${!COMPILERversion}/${ARCH}-${LABEL_COMPILER}/setup.sh
  export FC=`command -v gfortran`
  export CXX=`command -v g++`
  export CC=`command -v gcc`
  export ExtraCMakeOptions="${ExtraCMakeOptions}"
elif [[ $COMPILER == *clang* ]]; then
  clang34version=3.4
  clang35version=3.5
  clang36version=3.6
  clang37version=3.7
  clang38version=3.8
  clang39version=3.9
  clang50version=5.0
  clang501version=5.0.1
  clang600version=6.0.0binutils
  clang7version=7.0.0
  clang8version=8.0.0
  clang9version=9.0.0
  clang10version=10.0.0
  clang501binutilsversion=5.0.1binutils
  clang600binutilsversion=6.0.0binutils
  clang700binutilsversion=7.0.0binutils
  clang800binutilsversion=8.0.0binutils
  clang900binutilsversion=9.0.0binutils
  clang900libcppversion=9.0.0binutils
  clang1000binutilsversion=10.0.0binutils
  COMPILERversion=${COMPILER}version
  echo "COMPILERversion" ${COMPILERversion}
  clang34gcc=48
  clang35gcc=49
  clang37gcc=49
  clang38gcc=49
  clang39gcc=62
  clang50gcc=62 
  clang501gcc=62
  clang501binutilsgcc=62binutils
  clang600binutilsgcc=62binutils
  clang600gcc=62binutils
  clang700binutilsgcc=8binutils
  clang700gcc=8binutils
  clang800binutilsgcc=8binutils
  clang800gcc=8binutils  
  clang900binutilsgcc=9binutils
  clang900gcc=9binutils  
  clang1000binutilsgcc=10binutils
  clang1000gcc=10binutils  
  GCCversion=${COMPILER}gcc
  if [[ $COMPILER == *39* ]]; then
      source /cvmfs/sft.cern.ch/lcg/contrib/llvm/${!COMPILERversion}/${ARCH}-${LABEL_COMPILER}-gcc${!GCCversion}-opt/setup.sh
  else
     # Give priority to /cvmfs/sft.cern.ch/lcg/contrib/clang
     echo "\${\!COMPILERversion}" ${!COMPILERversion}
     COMPSETUP=/cvmfs/sft.cern.ch/lcg/contrib/clang/${!COMPILERversion}/${ARCH}-${LABEL_COMPILER}/setup.sh
     if test ! -r $COMPSETUP; then
        COMPSETUP=/cvmfs/sft.cern.ch/lcg/contrib/llvm/${!COMPILERversion}/${ARCH}-${LABEL_COMPILER}/setup.sh
     fi
     if [[ ${COMPILERversion} == clang900libcppversion ]]; then
        COMPSETUP=/cvmfs/sft.cern.ch/lcg/contrib/llvm/${!COMPILERversion}/${ARCH}-${LABEL_COMPILER}/setup-clanglibcpp.sh
     fi
     echo $COMPSETUP
     source $COMPSETUP
  fi
  export CC=`command -v clang`
  export CXX=`command -v clang++`
  export FC=`command -v gfortran`
elif [[ $COMPILER == *native* && $PLATFORM == *mac* ]]; then
  export LIBRARY_PATH=/usr/local/gfortran/lib
  export PATH=${PATH}:/usr/local/bin:/opt/X11/bin
  export CC=`command -v clang`
  export CXX=`command -v clang++`
  export FC=`command -v gfortran`
elif [[ $PLATFORM == *native* ]]; then
  export CC=`command -v gcc`
  export CXX=`command -v g++`
  export FC=`command -v gfortran`
elif [[ $COMPILER == *icc* ]]; then
  iccyear=2013
  icc14year=2013
  icc15year=2015
  COMPILERyear=${COMPILER}year
  iccgcc=4.9
  icc14gcc=4.9
  icc15gcc=4.9
  icc17gcc=6.2
  GCCversion=${COMPILER}gcc
  ARCH=$(uname -m)
  if [[ $COMPILER == *icc17* ]]; then
      source /cvmfs/sft.cern.ch/lcg/contrib/gcc/${!GCCversion}/${ARCH}-${LABEL_COMPILER}/setup.sh
      source /cvmfs/projects.cern.ch/intelsw/psxe/linux/all-setup.sh
      export CXX=`command -v icpc`
      export FC=`command -v gfortran`
      export LDFLAGS="-lirc -limf"
  else
      source /cvmfs/sft.cern.ch/lcg/contrib/gcc/${!GCCversion}/${ARCH}-slc6/setup.sh
      source /afs/cern.ch/sw/IntelSoftware/linux/setup.sh
      source /afs/cern.ch/sw/IntelSoftware/linux/${ARCH}/xe${!COMPILERyear}/bin/ifortvars.sh intel64
      source /afs/cern.ch/sw/IntelSoftware/linux/${ARCH}/xe${!COMPILERyear}/bin/iccvars.sh intel64
      export CXX=`command -v icc`
      export FC=`command -v ifort`
  fi
  export CC=`command -v icc`
#  export CXX=`command -v icc`
#  export FC=`command -v ifort`
  export ExtraCMakeOptions="${ExtraCMakeOptions} -Dvc=OFF"
fi

# Recalculate PLATFORM after the compiler has been properly setup
export PLATFORM=`$THIS/getPlatform.py`

# Go compiler - only centos7 and slc6 supported
if [[ $PLATFORM == *slc6* || $PLATFORM == *cc7* || $PLATFORM == *centos7* ]] && [[ -e /cvmfs/sft.cern.ch/lcg/contrib/go/latest ]]; then
    export PATH=/cvmfs/sft.cern.ch/lcg/contrib/go/latest/x86_64-$LABEL_COMPILER/bin:$PATH
    export GOROOT_BOOTSTRAP=/cvmfs/sft.cern.ch/lcg/contrib/go/latest/x86_64-$LABEL_COMPILER
fi
