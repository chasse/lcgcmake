//----------------------------------------------------------------------------------------------------------------------
// This declarative Jenkins pipeline encodes all the steps required for the release of a single platform.
// Other jobs may call this pipelinbe to execute the build, test and installation of a set platforms.
//
// Author: Pere Mato
//----------------------------------------------------------------------------------------------------------------------

//---Global Functions and Maps------(specific to this pileline)---------------------------------------------------------
def lcg
def getTarget() { return params.TARGET != 'all' ? params.TARGET : params.LCG_VERSION =~ 'ARM' ? 'top_packages' : 'all' }

pipeline {
  //---Global Parameters------------------------------------------------------------------------------------------------
  parameters {
    string(name:       'LCG_VERSION',          defaultValue: '99', description: 'LCG stack version to build')
    choice(name:       'COMPILER',             choices: ['gcc7', 'gcc8', 'gcc9', 'gcc10', 'clang8', 'clang10', 'native'])
    choice(name:       'BUILDTYPE',            choices: ['Release', 'Debug'])
    choice(name:       'LABEL',                choices: ['centos7', 'centos8', 'slc6', 'mac1015', 'arm64', 'ubuntu18', 'ubuntu20'])
    string(name:       'DOCKER_LABEL',         defaultValue: 'docker-host-noafs', description: 'Label for the the nodes able to launch docker images')
    string(name:       'ARCHITECTURE',         defaultValue: '', description: 'Complement of the architecture (instruction set)')

    string(name:       'LCG_INSTALL_PREFIX',   defaultValue: '', description: 'Location to look for already installed packages matching version and hash value. Leave blank for full rebuilds')
    string(name:       'LCG_ADDITIONAL_REPOS', defaultValue: '', description: 'Additional binary repository')
    string(name:       'LCG_EXTRA_OPTIONS',    defaultValue: '-DLCG_SOURCE_INSTALL=OFF;-DLCG_TARBALL_INSTALL=ON', description: 'Additional configuration options to be added to the cmake command')
    string(name:       'LCG_IGNORE',           defaultValue: '', description: 'Packages already installed in LCG_INSTALL_PREFIX to be ignored (i.e. full rebuild is required). The list is \'single space\' separated.')
    booleanParam(name: 'USE_BINARIES',         defaultValue: true, description: 'Use binaries in repositories')
    string(name:       'TARGET',               defaultValue: 'all', description: 'Target to build (all, <package-name>, ...)')
    choice(name:       'BUILDMODE',            choices: ['release', 'nightly'], description: 'Build mode')
    string(name:       'CVMFS_DEPLOYER',       defaultValue: 'cvmfs-sft', description: 'CVMFS publisher node label')

    booleanParam(name: 'CLEAN_INSTALLDIR',     defaultValue: true, description: 'Instruct to clean the install directory before building')
    booleanParam(name: 'DO_BUILD',             defaultValue: true, description: 'Do the build of the packages (otherwise only configure the stack)')
    booleanParam(name: 'RUN_TESTS',            defaultValue: true, description: 'Run the tests')
    booleanParam(name: 'CVMFS_INSTALL',        defaultValue: false, description: 'Install binaries in CVMFS')
    booleanParam(name: 'VIEWS_CREATION',       defaultValue: true, description: 'View creating while installing in CVMFS')
    booleanParam(name: 'LCGINFO_UPDATE',       defaultValue: true, description: 'Update the LCGinfo')
    booleanParam(name: 'RPM_CREATE',           defaultValue: false, description: 'RPMs creation')
    booleanParam(name: 'RPM_PUBLISH',          defaultValue: false, description: 'RPMs publish')
    choice(name:       'RPM_DRYRUN',           choices: ['no', 'yes'], description: 'RPMs publish dryrun')
    booleanParam(name: 'RPM_TEST',             defaultValue: false, description: 'RPMs tests')
    string(name:       'RPM_REPOSITORY',       defaultValue: '', description: 'RPM repository e.g /eos/project/l/lcg/www/lcgpackages/lcg/repo/7/LCG_97/ or empty')
  }
  //---Options----------------------------------------------------------------------------------------------------------
  options {
    buildDiscarder(logRotator( daysToKeepStr: '14' ))
    timestamps()
  }
  //---Additional Environment Variables---------------------------------------------------------------------------------
  environment {
    CDASH_TRACK = 'Release'
    CTEST_MODEL = 'Experimental'
    TARGET = getTarget()
    RELEASE_MODE = '1'
    db_pass = credentials('lcgsoft_db_password')
  }

  agent none

  stages {
    //------------------------------------------------------------------------------------------------------------------
    //---Environment Stage to load and set global parameters------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    stage('Environment') {
      agent  { label "$DOCKER_LABEL" }
      steps {
        script {
          //---Change the build name------------------------------------------------------------------------------------
          def BTYPE = [Release: 'opt', Debug: 'dbg']
          currentBuild.displayName = "#${BUILD_NUMBER}" + ' ' + params.LCG_VERSION + '-' + params.LABEL + '-' + params.COMPILER + '-' + BTYPE[params.BUILDTYPE]
          //---Load common variables and functions between several pilelines--------------------------------------------
          lcg = load 'lcgcmake/jenkins/Jenkinsfunctions.groovy'
        }
      }
    }
    //------------------------------------------------------------------------------------------------------------------
    //---Build stages---------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    stage('InDocker') {
      when {
        beforeAgent true
        expression { params.LABEL =~ 'centos|ubuntu|slc' }
      }
      agent {
        docker {
          alwaysPull true
          image "gitlab-registry.cern.ch/sft/docker/$LABEL"
          label "$DOCKER_LABEL"
          args  """-v /cvmfs:/cvmfs 
                   -v /ccache:/ccache 
                   -v /ec:/ec
                   -e SHELL
                   -e USER
                   -e container=docker
                   --net=host
                   --hostname ${LABEL}-docker
                """
        }
      }
      stages {
        stage('Build') {
          steps {
            script {
              lcg.buildPackages()
            }
          }
        }
        stage('CopyToEOS') {
          steps {
            script {
              lcg.copyToEOS()
            }
          }
        }
        stage('CreateRPMs') {
          when {
            expression { params.RPM_CREATE && 
                         params.LABEL =~ 'centos|slc' }
          }
          steps {
            script {
              lcg.createRPMS()
            }
          }
        }
      }
    }
    stage('InBareMetal') {
      when {
        beforeAgent true
        expression { params.LABEL =~ 'arm64|mac' }
      }
      agent {
        label "$LABEL"
      }
      stages {
        stage('Build') {
          steps {
            script {
              lcg.buildPackages()
            }
          }
        }
        stage('CopyToEOS') {
          steps {
            script {
              lcg.copyToEOS()
            }
          }
        }
      }
    }
    //------------------------------------------------------------------------------------------------------------------
    //---CVMFS Installation stage---------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    stage('MacPreinstall') {
      when {
        beforeAgent true
        expression { params.CVMFS_INSTALL && params.LABEL =~ 'mac' }
      }
      agent {
        label "$LABEL"
      }
      steps {
        script {
          lcg.preinstallMacOS()
        }
      }
    }
    stage('CVMFSInstall') {
      when {
        beforeAgent true
        expression { params.CVMFS_INSTALL }
      }
      agent {
        label "$CVMFS_DEPLOYER"
      }
      steps {
        script {
          lcg.installCVMFS()
        }
      }
    }
    stage('LCGinfo') {
      when {
        beforeAgent true
        expression { params.LCGINFO_UPDATE && !(params.LCG_VERSION =~ 'dev') }
      }
      agent {
        label "$DOCKER_LABEL"
      }
     steps {
        script {
          lcg.cloneRepository('lcginfo', 'sft')
          if(params.CVMFS_INSTALL) {
            lcg.waitForCVMFS()
          }
          docker.withRegistry( 'https://gitlab-registry.cern.ch', '7c3a9bea-6657-46fc-901d-bef5c7e0061c' ) {
            lcg.publishINFO()
          }
        }
      }
    }
    //----------------------------------------------------------------------------------------------------------------------
    //---Finalize the RPM stages--------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------
    stage('PublishRPMs') {
      when {
        beforeAgent true
        expression { params.RPM_PUBLISH && params.LABEL =~ 'centos|slc' }
      }
      agent {
        label 'centos7-physical'
      }
      steps {
        script {
          lcg.publishRPMS()
        }
      }
    }
    stage('TestRPMs') {
      when {
        beforeAgent true
        expression { params.RPM_TEST && params.LABEL =~ 'centos|slc' }
      }
      agent {
          docker {
          alwaysPull true
          image "gitlab-registry.cern.ch/sft/docker/$LABEL"
          label "$DOCKER_LABEL"
          args  "-u root"
        }
 
      }
      steps {
        script {
          lcg.testRPMS()
        }
      }
    }
  }
}

