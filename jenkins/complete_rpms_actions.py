#!/usr/bin/python3
from __future__ import print_function
import argparse
import os
import sys
import re
import subprocess
import shlex
from pathlib import Path
import itertools
from time import sleep

from common_parameters import tmparea, officialarea, archivearea, updatesarea, eosmgmurl

# Copy files to EOS, retrying multiple times in case of network glitches
def copy_to_eos(from_mask, to, dryrun):
    max_attempts = 5
    timeout_seconds = 1
    # from_ = from_.replace('project/l', 'project-l')
    for from_file in Path('/').glob(from_mask.lstrip('/')):
        print("Copy", from_file, "to", to, end=' ')
        if dryrun:
            print("\t[DRYRUN]")
        else:
            command = "xrdcp -s %s %s" % (from_file, to)
            
            for attempt in range(1, max_attempts + 1):
                try:
                    subprocess.check_output(shlex.split(command), stderr=subprocess.STDOUT)
                except subprocess.CalledProcessError as e:
                    if e.returncode == 54:
                        print("\t[EXISTS]")
                        break
                    else:
                        print("Attempt {0} of {1} failed!".format(attempt, max_attempts))
                        print("\t[ERROR {0}]".format(e.returncode))
                        print(e.output.decode("utf-8"))
                else:
                    print("\t[OK]")
                    break
                sleep(timeout_seconds)
                timeout_seconds *= 2

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("version", help="LCG release version, e.g. 88 ot 97python3")
    parser.add_argument("repo", help="Path to target RPM repository, ignored in legacy mode")
    parser.add_argument("platform", help="Platform, ignored in legacy mode")
    parser.add_argument("-n", "--dryrun", default=False, action="store_true")
    parser.add_argument("--legacy", default=False, action="store_true", help="Legacy mode: for LCG_96 and older")
    args = parser.parse_args()

    version = args.version
    platform = args.platform
    dryrun = args.dryrun
    if args.legacy:
        existing_rpms = itertools.filterfalse(
            lambda r: r.name.startswith('LCG'),
            itertools.chain(
                Path(archivearea).glob('*{0}*.rpm'.format(platform)),
                Path(updatesarea).glob('*{0}*.rpm'.format(platform))
            )
        )
        targetarea = updatesarea
        platform = '_'
        arch = '_'
    else:
        targetarea = args.repo.rstrip('/')
        existing_rpms = itertools.filterfalse(
            lambda r: r.name.startswith('LCG'),
            itertools.chain(
                Path(archivearea).glob('*{0}*.rpm'.format(platform)),
                Path(updatesarea).glob('*{0}*.rpm'.format(platform)),
                Path(targetarea).glob('*{0}*.rpm'.format(platform)),
            )
        )
        ac = platform.split('_')
        arch = ac[0] + '_' + ac[1] + '_' + ac[2] + '-'

    print("Copy RPMs from {0}/LCG_{1} to {2}, archive area {3}".format(tmparea, version, targetarea, archivearea))

    lcg_subdirectory = "LCG_" + version
    if args.legacy:
        lcg_subdirectory += "release"        
        
    # lcg_directory = targetarea + lcg_subdirectory
    # os.environ['EOS_MGM_URL'] = eosmgmurl

    list_existing = set()
    list_new = set()

    # os.makedirs(lcg_directory, exist_ok=True)

    print("Collecting existing prefixes...")
    for j in existing_rpms:
        if j.is_dir():
            continue

        rpm_prefix = j.name.split("-1.0.0")[0]
        list_existing.add(rpm_prefix)

    print(f"Collected {len(list_existing)} prefixes")

    print("Collecting incoming prefixes from", str(Path(tmparea) / f"LCG_{version}"))
    for f in itertools.chain(
        (Path(tmparea) / f"LCG_{version}").glob(f'[!.]*{platform}*.rpm'),
        (Path(tmparea) / f"LCG_{version}").glob(f'[!.]*{arch}*.rpm')
    ):
        if f.is_dir():
            continue

        if f.name.startswith(f'LCG_{version}') or not f.name.startswith('LCG_'):
            rpm_prefix = f.name.split("-1.0.0")[0]
            list_new.add(rpm_prefix)

    print(f"Collected {len(list_new)} prefixes")

    list_to_copy = list_new.difference(list_existing)

    print(f"Will copy {len(list_to_copy)} new prefixes")

    target = {True: f"{targetarea}/{lcg_subdirectory}/", False: f"{targetarea}/"}
    if not args.legacy:
        target[False] = f"{targetarea}/Packages/"

    for d in target.values():
        if not args.dryrun:
            os.makedirs(d, exist_ok=True)

    for t in list_to_copy:
        copy_to_eos(str(Path(tmparea) / f"LCG_{version}" / f'{t}*.rpm'), eosmgmurl + target[t.startswith('LCG')], dryrun)


if __name__ == "__main__":
    main()
