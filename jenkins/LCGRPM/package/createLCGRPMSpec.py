#!/usr/bin/python3
"""
Script to create the LCGCMT RPM Spec file based on the JSON configuration
produced by the build itself.
See usage method and command help for use.

"""
from __future__ import print_function

import copy
import logging
import os
import optparse
import re
import sys
from string import Template

log = logging.getLogger()
log.setLevel(logging.INFO)

CONTAINER_LIST = [  ]
LCGNAME = "LCG"
LCGPREFIX="/opt/lcg"
RPMPREFIX="/opt/lcg"
IGNORE_PKG = [ "lhapdfsets", "lhapdf6sets"  ]

	
###############################################################################
# Classes representing the various types of RPM
###############################################################################

# Class representing a RPM
###############################################################################
class Rpm(object):
    """ Base class representing a nomal RPM package, with files located in
    lcg/<name>/<version-with-hash> """
    def __init__(self, lcgcmt_version, name, version, config, dirname,
                 hash, hat = None, type="externals", targetLCGPrefix=LCGPREFIX):
        self.lcgcmt_version = lcgcmt_version
        self.name = name
        self.version = version
        self.config = config
        self.hat = hat
        self.dirname = dirname
        self.hash = hash
        self.namehash = name + "-" + hash
        self.deps = list()
        self.dict = None
        self.getRPMName = self.getHashRPMName
        self.isMetaRPM = False
        # Get the top dir for the package, we need to check whether there is a CMTCONFIG
        if self.dirname.endswith(self.config):
            self.namewithhat = os.path.join(*self.dirname.split(os.sep)[:-2])
        else:
            self.namewithhat = os.path.join(*self.dirname.split(os.sep)[:-1])
        # Remove the ./ at the beginning of the directory name, as this interferes with the
        # count of '/' which is used to determine whether the package is in a hat.
        self.namewithhat = self.namewithhat.lstrip("./")
        self.metadata = None
        self.sourceMetadataFileName = None
        self.metadataFileName = "LCG_%s_%s.txt" % (type, config)
        self.targetLCGPrefix = targetLCGPrefix
        self.externalType = type

    def getRPMDependencies(self):
        """ Looking up the dependencies from the dictionary and return a list of RPM opbjects """
        rpm_packages = []
        for d in self.dependencies:
            # First checking the data
            if self.dict == None:
                raise Exception("%s %s Looking up dependency %s but no dictionary available" % (self.getRPMName(), self.getRPMVersion(), d))
            # Looking up the rpm itself
            drpm = None
            try:
                drpm = self.dict[d]
            except KeyError:
                print("Could not find: %s" % d)
                print("In: ", list(self.dict.keys()))
            if drpm == None:
                raise Exception("%s %s Looking up dependency %s but not in dictionary" % (self.getRPMName(), self.getRPMVersion(), d))
            rpm_packages.append(drpm)
        return rpm_packages

    def getLCGRPMName(self):
        """ Returns  name of the LCG_<version>_<external> RPM """
        pre = self.name
        if self.hat != None:
            pre = self.hat + "_" + self.name

        post = (self.version + "_" + self.config).replace("-", "_")
        return LCGNAME + "_" + self.lcgcmt_version + "_" + pre + "_" + post

    def getMetadataRPMName(self):
        """ Returns the name of the metadata RPM that should be referenced """
        post = self.config.replace("-", "_")
        return LCGNAME + "Meta_" + self.lcgcmt_version + "_" + type  + "_" + post
    
    def getMetadataFileName(self, postfix=""):
        """ Return the filename for the metadata file """
        return os.path.join(self.targetLCGPrefix,  LCGNAME + "_" + self.lcgcmt_version, self.metadataFileName + postfix)

    def getMetadataDirName(self):
        """ Return the full file name for the LCG metadata file """
        return os.path.join(self.targetLCGPrefix,  LCGNAME + "_" + self.lcgcmt_version)
    
    def getHashRPMName(self):
        """ Get the name of the RPM including the unique hash"""
        pre = self.namehash
        if self.hat != None:
            pre = self.hat + "_" + self.namehash
        post = (self.version + "_" + self.config).replace("-", "_")
        return pre + "_" + post

    def getRPMVersion(self):
        return "1.0.0"

    def getSourcePathWithLCG(self):
        ''' Location in the LCG Install area '''
        #return os.path.join(LCGNAME +"_" + self.lcgcmt_version, self.dirname , self.version, self.config)
        return os.path.join(LCGNAME +"_" + self.lcgcmt_version, self.dirname)

    def getSourcePath(self):
        ''' Location in the LCG Install area '''
        return self.dirname

    def getTargetPath(self):
        ''' Location to which is should go after install '''
        return os.path.join(self.namewithhat, self.version + "-" + self.hash, self.config)

    def getTargetLCGCMTPath(self):
        ''' Location to which is should go iafter install '''
        return os.path.join(LCGNAME +"_" + self.lcgcmt_version, self.namewithhat, self.version)

    def __repr__(self):
        return self.getRPMName()

    def __str__(self):
        strg = "RPM: %s-%s\n" % (self.getRPMName(), self.getRPMVersion())
        return strg

    def prepareRPMDescription(self):
        """ Prepare the description of this package to be
        inserted in the global SPEC file """
        rpm_desc = """
%%description -n  %s
%s %s
""" % (self.getRPMName(), self.getRPMName(), self.getRPMVersion())
        return rpm_desc

    def prepareRPMPost(self, lcg_path=None):
        """ Prepare the post scriptlets to be added """
        # Now checking for the post install file
        packtoppath = self.getTargetPath()
        rpm_post = '''
%%post -n  %(rpmname)s

if [ "$RPM_INSTALL_PREFIX" ]; then
PREFIX=$RPM_INSTALL_PREFIX
else
PREFIX=%%{prefix}
fi

echo "Checking for post install in ${PREFIX}/%(installdir)s" 
if [[ -f ${PREFIX}/%(installdir)s/.post-install.sh ]]; then
echo "Invoking post install ${PREFIX}/%(installdir)s/.post-install.sh"
${PREFIX}/%(installdir)s/.post-install.sh %%{LCGCMTROOT}
fi

''' % { 'rpmname':self.getRPMName(), 'installdir':packtoppath,  }
        return rpm_post
    
    def getInstallCommands(self, lcg_path):
        """ Prepares the list of installation command used to prepare the package """
        spec =  "mkdir -p ${RPM_BUILD_ROOT}%s/%s\n" % (lcg_path,  self.getTargetPath())
        spec += "rsync -ar %%{LCGCMTROOT}/%s/ ${RPM_BUILD_ROOT}%s/%s\n" % \
            (self.getSourcePath(), lcg_path, self.getTargetPath())
        return spec

    def getFiles(self, lcg_path):
        """ Prepares the files part for the SPEC for this package """
        rpm_files = "\n%%files -n  %s\n" % (self.getRPMName())
        rpm_files += "%defattr(-,root,root)\n"
        rpm_files += "%s/%s\n" % (lcg_path, self.getTargetPath())
        rpm_files += "\n"
        return rpm_files

    def prepareRPMPackage(self):
        """ Prepare the package entry of this package to be
        inserted in the global SPEC file """
        rpm_packages = Template("""
%package -n $n
Version:$v
Group:LCG
Summary: LCG $n $v
AutoReqProv: no
""").substitute(n=self.getRPMName(), v=self.getRPMVersion(), )
        # Now adding the list of Provides for this package
        rpm_packages += "Provides: %s = %s\n" % (self.name, self.version)
        rpm_packages += "Provides: /bin/sh\n"

        # Now adding the list of requirements
        for drpm in self.getRPMDependencies():
            rpm_packages += "Requires: %s\n" % drpm.getRPMName()
        return rpm_packages

    def checkDirExists(self, lcg_dir):
        """ Checks if the external is actually installed in the lcg_dir specified """
        return os.path.exists(os.path.join(lcg_dir, self.getSourcePath() ))

    def checkIfSymlink(self, lcg_dir):
        """ Checks if there is a concrete install or just a symlink """
        return os.path.islink(os.path.join(lcg_dir, self.getSourcePath() ))

# Class representing a RPM containing the links to the installed version
###############################################################################
class RpmLink(Rpm):
    """ Represents the RPM containing the link to the real package """
    
    def __init__(self, lcgcmt_version, name, version, config, dirname, hash, hat = None):
        self.realName = name
        super(RpmLink, self).__init__(lcgcmt_version, name, version, config, dirname, hash, hat)
        self.getRPMName = self.getLCGRPMName

    def getInstallCommands(self, lcg_path):
        """ Prepares the list of installation command used to prepare the package """
        linkpath = "${RPM_BUILD_ROOT}%s/%s" %  (lcg_path,  self.getTargetLCGCMTPath())
        rpm_common = "mkdir -p %s\n" % linkpath
        updircount = 3 + self.namewithhat.count("/")
        updir = []
        for i in range(updircount):
            updir.append('..')
        rpm_common += "cd %s &&  ln -s %s/%s\n" % (linkpath, "/".join(updir),  self.getTargetPath())
        return rpm_common

    def getFiles(self, lcg_path):
        """ Prepares the files part for the SPEC for this package """
        rpm_files = "\n%%files -n  %s\n" % (self.getRPMName())
        rpm_files += "%defattr(-,root,root)\n"
        rpm_files += "%s/%s\n" % (lcg_path, os.path.join(self.getTargetLCGCMTPath(), self.config))
        rpm_files += "\n"
        return rpm_files

    def prepareRPMPost(self, lcg_path=None):
        """ Prepare the post scriptlets to be added """
        rpm_post = ""

        # Now checking for the post install file
        packtoppath = os.path.join(self.getTargetLCGCMTPath(), self.config)
        rpm_post += '''
%%post -n  %(rpmname)s''' % { 'rpmname':self.getRPMName()  }

        if self.metadata != None:
            tmpDirName = self.getMetadataDirName().replace(RPMPREFIX, "")
            tmpFileName = self.getMetadataFileName(".installed").replace(RPMPREFIX, "")
            rpm_post += """

mkdir -p ${RPM_INSTALL_PREFIX}%s
echo \"Creating metadata file in  ${RPM_INSTALL_PREFIX}%s\"
echo \"%s\" >> ${RPM_INSTALL_PREFIX}%s

""" % (tmpDirName, tmpDirName, 
       self.metadata, tmpFileName)

            rpm_post += """
%%postun -n  %s
if [[ -f ${RPM_INSTALL_PREFIX}%s ]]; then
  echo \"Modifying metadata file ${RPM_INSTALL_PREFIX}%s\"
  mv ${RPM_INSTALL_PREFIX}%s ${RPM_INSTALL_PREFIX}%s.old
  grep -v \"%s\" ${RPM_INSTALL_PREFIX}%s.old > ${RPM_INSTALL_PREFIX}%s
fi

""" % (self.getRPMName(),
       tmpFileName,
       tmpFileName,
       tmpFileName,
       tmpFileName, 
       self.metadata,
       tmpFileName,
       tmpFileName
       )
        return rpm_post

    def prepareRPMPackage(self):
        """ Prepare the package entry of this package to be
        inserted in the global SPEC file """
        rpm_packages = Template("""
%package -n $n
Version:$v
Group:LCG
Summary: LCG $n $v
AutoReqProv: no
""").substitute(n=self.getRPMName(), v=self.getRPMVersion(), )
        # Now adding the list of Provides for this package
        rpm_packages += "Requires: %s\n" % self.getHashRPMName()
        rpm_packages += "Requires: %s\n" % self.getMetadataRPMName()
        # Now adding the list of requirements
        for drpm in self.getRPMDependencies():
            rpm_packages += "Requires: %s\n" % drpm.getLCGRPMName()
        return rpm_packages


# Class representing a RPM containing the links to the installed version
###############################################################################
class RpmContribLink(Rpm):
    """ Represents the RPM containing the link to the real package """
    
    def __init__(self, lcgcmt_version, name, version, config, dirname, hash,
                 hat = None, type="externals", targetLCGPrefix=LCGPREFIX):
        # We can't inherit from parent's we can't use the config that is passed
        self.realName = name
        self.lcg_cmtconfig = config
        self.lcgcmt_version = lcgcmt_version
        self.name = name
        self.version = version
        self.hat = hat
        self.dirname = dirname
        self.hash = hash # IGNORED
        self.namehash = self.name
        # Overridding the config with the plaform found in the dirname
        # (we have the real platform instead of the CMTCONFIG there)
        self.config = os.path.basename(self.dirname)
        self.namehash = name
        self.deps = list()
        self.dict = None
        self.isMetaRPM = False
        # Get the top dir for the package, we need to check whether there is a CMTCONFIG
        if self.dirname.endswith(self.config):
            self.namewithhat = os.path.join(*self.dirname.split(os.sep)[:-2])
        else:
            self.namewithhat = os.path.join(*self.dirname.split(os.sep)[:-1])
        self.metadata = None
        self.sourceMetadataFileName = None
        self.metadataFileName = "LCG_%s_%s.txt" % (type, config)
        self.targetLCGPrefix = targetLCGPrefix
        self.externalType = type        
        self.getRPMName = self.getLCGRPMName
        self.dependencies = []

    def getTargetPath(self):
        ''' Location to which is should go after install '''
        return os.path.join(self.namewithhat, self.version, self.config)


    def getInstallCommands(self, lcg_path):
        """ Prepares the list of installation command used to prepare the package """
        linkpath = "${RPM_BUILD_ROOT}%s/%s" %  (lcg_path,  self.getTargetLCGCMTPath())
        rpm_common = "mkdir -p %s\n" % linkpath
        updircount = 3 + self.namewithhat.count("/")
        updir = []
        for i in range(updircount):
            updir.append('..')
        rpm_common += "cd %s &&  ln -s %s/%s\n" % (linkpath, "/".join(updir),  self.getTargetPath())
        return rpm_common

    def getFiles(self, lcg_path):
        """ Prepares the files part for the SPEC for this package """
        rpm_files = "\n%%files -n  %s\n" % (self.getRPMName())
        rpm_files += "%defattr(-,root,root)\n"
        rpm_files += "%s/%s\n" % (lcg_path, os.path.join(self.getTargetLCGCMTPath(), self.config))
        rpm_files += "\n"
        return rpm_files

    def prepareRPMPost(self, lcg_path=None):
        """ Prepare the post scriptlets to be added """
        rpm_post = ""
        if self.metadata != None:
            tmpDirName = self.getMetadataDirName().replace(RPMPREFIX, "")
            tmpFileName = self.getMetadataFileName(".installed").replace(RPMPREFIX, "")
            rpm_post = """
%%post -n  %s
mkdir -p ${RPM_INSTALL_PREFIX}%s
echo \"Creating metadata file in  ${RPM_INSTALL_PREFIX}%s\"
echo \"%s\" >> ${RPM_INSTALL_PREFIX}%s

""" % (self.getRPMName(), tmpDirName, tmpDirName, 
       self.metadata, tmpFileName)

            rpm_post += """
%%postun -n  %s
if [[ -f ${RPM_INSTALL_PREFIX}%s ]]; then
  echo \"Modifying metadata file ${RPM_INSTALL_PREFIX}%s\"
  mv ${RPM_INSTALL_PREFIX}%s ${RPM_INSTALL_PREFIX}%s.old
  grep -v \"%s\" ${RPM_INSTALL_PREFIX}%s.old > ${RPM_INSTALL_PREFIX}%s
fi

""" % (self.getRPMName(),
       tmpFileName,
       tmpFileName,
       tmpFileName,
       tmpFileName, 
       self.metadata,
       tmpFileName,
       tmpFileName
       )
        return rpm_post

    def prepareRPMPackage(self):
        """ Prepare the package entry of this package to be
        inserted in the global SPEC file """
        rpm_packages = Template("""
%package -n $n
Version:$v
Group:LCG
Summary: LCG $n $v
AutoReqProv: no
""").substitute(n=self.getRPMName(), v=self.getRPMVersion(), )
        # Now adding the list of Provides for this package
        rpm_packages += "Requires: %s\n" % self.getHashRPMName()
        # Now adding the list of requirements
        for drpm in self.getRPMDependencies():
            rpm_packages += "Requires: %s\n" % drpm.getLCGRPMName()
        return rpm_packages


    def getHashRPMName(self):
        """ Get the name of the RPM including the unique hash"""
        # Overriding method to remove all references to hash name
        pre = self.name
        if self.hat != None:
            pre = self.hat + "_" + self.name
        post = (self.version + "_" + self.config).replace("-", "_")
        return pre + "_" + post


# Class representing a RPM without a hash in the name
###############################################################################
class RpmShared(Rpm):
    """ RPM with shared files (no cmtconfig) and no hash in name """
    def __init__(self, lcgcmt_version, name, version, config, dirname,
                 hash, hat = None, type="externals", targetLCGPrefix=LCGPREFIX):
        super(RpmShared, self).__init__(lcgcmt_version, name, version, config, dirname, hash, hat)
   
    def getHashRPMName(self):
        """ Get the name of the RPM including the unique hash"""
        # Overriding method to remove all references to hash name
        pre = self.name
        if self.hat != None:
            pre = self.hat + "_" + self.name
        post = self.version
        return pre + "_" + post

    def getLCGRPMName(self):
        """ Override to as the LCG_<version>... RPM does NOT exist """
        return self.getHashRPMName()

    def getRPMVersion(self):
        return "1.0.0"

    def getTargetPath(self):
        ''' Location to which is should go after install '''
        # Overriding method to remove all references to hash name
        return os.path.join(self.namewithhat, self.version)

# Class representing a contrib RPM
###############################################################################
class RpmContrib(Rpm):
    """ RPM for contrib packages """
    def __init__(self, lcgcmt_version, name, version, config, dirname,
                 hash, hat = None, type="externals", targetLCGPrefix=LCGPREFIX):
        super(RpmContrib, self).__init__(lcgcmt_version, name, version, config, dirname, hash, hat)
        # Overridding the config with the plaform found in the dirname
        # (we have the real platform instead of the CMTCONFIG there)
        self.lcg_cmtconfig = config
        self.config = os.path.basename(self.dirname)

    def getHashRPMName(self):
        """ Get the name of the RPM including the unique hash"""
        # Overriding method to remove all references to hash name
        pre = self.name
        if self.hat != None:
            pre = self.hat + "_" + self.name
        post = (self.version + "_" + self.config).replace("-", "_")
        return pre + "_" + post

    def getLCGRPMName(self):
        """ Override to as the LCG_<version>... RPM does NOT exist """
        return self.getHashRPMName()

    def getRPMVersion(self):
        return "1.0.0"

    def getTargetPath(self):
        ''' Location to which is should go after install '''
        # Overriding method to remove all references to hash name
        return os.path.join(self.namewithhat, self.version)


# Class representing a RPM containing the links to the installed version
###############################################################################
class RpmMetadata(Rpm):
    """  """
    def __init__(self, lcgcmt_version, config, type, targetLCGPrefix=LCGPREFIX ):
        self.lcgcmt_version = lcgcmt_version
        self.name = "metadata"
        self.version = None
        self.hat = None
        self.config = config
        self.isMetaRPM = False
        self.sourceMetadataFileName = None
        self.metadaFileName = "LCG_%s_%s.txt" % (type, config)
        self.externalType = type
        self.targetLCGPrefix = targetLCGPrefix
        self.getRPMName = self.getMetadataRPMName
        self.getLCGRPMName = self.getMetadataRPMName
        self.metadataFileName = "LCG_%s_%s.txt" % (type, config)

    def getInstallCommands(self, lcg_path):
        """ Prepares the list of installation command used to prepare the package """
        dirpath = "${RPM_BUILD_ROOT}%s" %  os.path.dirname(self.getMetadataFileName())
        rpm_common = "mkdir -p %s\n" % dirpath
        sourcefile = "%%{LCGCMTROOT}/%s" % os.path.basename(self.getMetadataFileName())
        if self.sourceMetadataFileName != None:
            sourcefile = self.sourceMetadataFileName
        rpm_common += "cp %%{LCGCMTROOT}/%s  ${RPM_BUILD_ROOT}%s\n" % (sourcefile,  self.getMetadataFileName())
        return rpm_common
    
    def getFiles(self, lcg_path):
        """ Prepares the files part for the SPEC for this package """
        rpm_files = "\n%%files -n  %s\n" % (self.getRPMName())
        rpm_files += "%defattr(-,root,root)\n"
        rpm_files += "%s\n" % self.getMetadataFileName()
        rpm_files += "\n"
        return rpm_files

    def getSourcePath(self):
        ''' Location in the LCG Install area '''
        return  self.getMetadataFileName()

    def prepareRPMPost(self, lcg_path=None):
        """ Prepare the post scriptlets to be added """
        rpm_post = ""
        return rpm_post
   
    def prepareRPMPackage(self):
        """ Prepare the package entry of this package to be
        inserted in the global SPEC file """
        rpm_packages = Template("""
%package -n $n
Version:$v
Group:LCG
Summary: LCG $n $v
AutoReqProv: no
""").substitute(n=self.getRPMName(), v=self.getRPMVersion(), )
        # Now adding the list of Provides for this package
        return rpm_packages

    def checkDirExists(self, lcg_dir):
        """ Checks if the external is actually instaleld in the lcg_dir specified
        No need! the file is the source for the packaging, and the location depends
        on the the options passed, so disabling the check"""
        return True
    
# Class representing a RPM containing all the links to the installed version
###############################################################################
class LCGRpm(object):
    """ Base class representing a RPM containing the installed version of LCG with all links
    to installed packages """
    def __init__(self, lcgcmt_version, config):
        self.lcgcmt_version = lcgcmt_version
        self.name = "LCG"
        self.config = config
        self.deps = list()
        self.dict = None
        self.isMetaRPM = True
        
    def getRPMName(self):
        pre = self.name + "Link"
        post = (self.lcgcmt_version + "_" + self.config).replace("-", "_")
        return pre + "_" + post

    def getRPMVersion(self):
        return "1.0.0"

    def getTargetPath(self):
        ''' Location to which is should go after install '''
        return self.name + '_' + self.lcgcmt_version

    def __str__(self):
        strg = "RPM: %s-%s\n" % (self.getRPMName(), self.getRPMVersion())
        return strg

    def prepareRPMDescription(self):
        """ Prepare the description of this package to be
        inserted in the global SPEC file """
        rpm_desc = """
%%description -n  %s
%s %s
""" % (self.getRPMName(), self.getRPMName(), self.getRPMVersion())
        return rpm_desc

    def prepareRPMPost(self, lcg_path=None):
        """ Prepare the post scriptlets to be added """
        rpm_post = ""
        return rpm_post
    

    def prepareRPMPackage(self):
        """ Prepare the package entry of this package to be
        inserted in the global SPEC file """
        rpm_packages = Template("""
%package -n $n
Version:$v
Group:LCG
Summary: LCG $n $v
AutoReqProv: no
""").substitute(n=self.getRPMName(), v=self.getRPMVersion(), )

        for drpm in self.getRPMDependencies():
            rpm_packages += "Requires: %s\n" % drpm.getRPMName()
        return rpm_packages


###############################################################################
# Methods to parse/filter the Metadeta files
###############################################################################

# Directory configuration
###############################################################################
def loadLCGMetaData(filename):
    """ Load the JSON file and check the basic structure """
    # First loading the JSON
    with open(filename) as inputfile:
        inputdata = inputfile.readlines()

    packages = {}
    for line in inputdata:
        if re.match("^\s*PLATFORM:", line) or re.match("^\s*VERSION:", line):
            continue
        
        # Parse the file
        # Ignore commments
        if re.match("^\s*#", line):
            logging.debug("Found comment: %s" % line)
            continue
        # Look for specific compiler line
        if re.match("^\s*COMPILER", line):
            logging.debug("Found COMPILER: %s" % line)
            continue
        # Parse proper line
        # name-hash; package name; version; hash; full directory name; comma separated dependencies
        (name, hash, version, dirname, deps) = line.split(";")
        name = name.strip()
        hash = hash.strip()
        version = version.strip()
        dirname = dirname.strip()
        namehash = name + "-" + hash
        #log.debug((namehash, name, version, hash, dirname, deps))
        pdata = {}
        pdata['name'] = name
        pdata['namehash'] = namehash
        pdata['hash'] = hash
        pdata['version'] = version
        pdata['dirname'] = dirname
        pdata['dependencies'] = [ e.strip() for e in deps.strip().split(",") if e != "" ]
        pdata['metadata'] = line.strip()
        pdata['metadataFileName'] = os.path.basename(filename)
        packages[name + "-" + hash] = pdata
        log.debug(pdata)
        
    data = {}
    data['packages'] = packages
    return  data

# Directory configuration
###############################################################################
def loadContribMetaData(filename):
    """ Load the JSON file and check the basic structure """
    # First loading the JSON
    with open(filename) as inputfile:
        inputdata = inputfile.readlines()

    packages = {}
    for line in inputdata:
        # Parse the file
        # Ignore commments
        if re.match("^\s*#", line):
            logging.debug("Found comment: %s" % line)
            continue

        # name-hash; package name; version; hash; full directory name; comma separated dependencies
        (name, hash, version, dirname, deps) = line.split(";")
        name = name.strip()
        version = version.strip()
        dirname = dirname.strip()
        hash = ""
        namehash = name + "-"

        pdata = {}
        pdata['name'] = name
        pdata['version'] = version
        pdata['hash'] = hash
        pdata['dirname'] = dirname
        pdata['dependencies'] = [ e.strip() for e in deps.strip().split(",") if e != "" ]
        pdata['metadata'] = line.strip()
        pdata['metadataFileName'] = os.path.basename(filename)
        packages[name] = pdata
        log.debug(pdata)
        
    return  packages

# Reading LCGCMT version and platform from file
###############################################################################
def loadLCGCMTVersionPlatform(filename):
    """ Load the JSON file and check the basic structure """
    # First loading the JSON
    with open(filename) as inputfile:
        inputdata = inputfile.readlines()

    platform = None
    version = None
    for line in inputdata:
        m = re.match("^\s*PLATFORM:\s*([a-zA-Z0-9_\-]*)", line)
        if m != None:
            platform = m.group(1)

        m = re.match("^\s*VERSION:\s*([a-zA-Z0-9_\-]*)", line)
        if m != None:
            version = m.group(1)

        if version != None and platform != None:
            break
    log.debug("Platform: %s, Version: %s" % (platform, version))
    return (platform, version)

# Remove container and clean dependencies
###############################################################################
def fixLCGCMTData(data):
    """ Convert the file to something useable by LCGCMT users """

    packages = data["packages"]

    # Check which directories are used several times
    # usecount = dict()
    # for k in packages.values():
    #     dirname = k['dirname']
    #     usecount[dirname] = usecount.get(dirname, 0) + 1
    # for k in  usecount.keys():
    #     print "%s   -> %d" % (k, usecount[k])
    # containers = [ k for k in usecount.keys() if usecount[k] > 1 ]
    # print containers
    newpackages = dict()
    for k, pack in packages.items():
        name = pack["name"]
        version = pack["version"]
        dirname = pack["dirname"]

        # Ignoring packages contained in a container
        if (name != dirname and dirname in CONTAINER_LIST) \
               or name in IGNORE_PKG:
            log.debug("Ignoring package " + name + " " + version + " in " + dirname)
            continue
        
        # Now filtering the dependencies
        log.debug("Filtering deps for " + name + " " + version + " in " + dirname)
        deps =  pack["dependencies"]
        newdeps = []
        for d in deps:
            log.debug("Identifying dependency: %s" % d)
            if len([ip for ip in IGNORE_PKG if d.startswith(ip)]) > 0:
                continue
            newdeps.append(d)
            log.debug("Added dependency: %s" % d)

        # Copying and fixing deps
        newpack = copy.deepcopy(pack)
        newpack['dependencies'] = newdeps
        newpackages[k] = newpack

    newdata = dict()
    newdata['packages'] = newpackages
    log.debug("Finished fixing the dependencies")
    return newdata

# Prepare the container of RPM objects to be processed
###############################################################################
def prepareRPMDict(data, lcgcmt_version, platform, type, metadataFileName=None,
                   contribs=None, contriblinks=None):
    """ Extract a list of RPM Objects based on the LCGCMT packages """
    rpmDict= dict()    
    packagesMap = data['packages']
    for k, d in packagesMap.items():
        name = d["name"]
        version = d["version"]
        dirname = d["dirname"]
        if not name:
            log.error("Missing name for key: %s" % k)
            name = k
        # Doublechecking...
        deptype = "NORMAL"
        if name != dirname \
           and dirname in CONTAINER_LIST:
            #log.debug("Ignoring package " + name + " " + version + " in " + dirname)
            deptype = "SUBDEP"

        log.debug("Adding package " + name + " " + version + " type:" + deptype)
        # LCGCMT is different...
        if name == "LCGCMT":
            dictkey = name + "-" + d['hash']
            # We need to remove the cmtconfig from the LCGCMT dirname
            if dirname.endswith(platform):
                dirname = os.path.join(*dirname.split(os.sep)[0:-1])
            r = RpmShared(lcgcmt_version, name, version , platform, dirname, d['hash'])
            r.metadata = d['metadata']
            r.metadataFileName = d['metadataFileName']
            r.dependencies = list(d["dependencies"])
            r.dict = rpmDict
            rpmDict[dictkey] = r
        else:        
            # Dependencies are stated in the form <name>-<hash>
            dictkey = name + "-" + d['hash']
            r = Rpm(lcgcmt_version, name, version , platform, dirname, d['hash'])
            r.metadata = d['metadata']
            r.metadataFileName = d['metadataFileName']
            r.dependencies = list(d["dependencies"])
            for k, v in contribs.items():
                r.dependencies.append(v.name)
            r.dict = rpmDict
            rpmDict[dictkey] = r

            log.debug("Adding link package " + name + " " + version + " type:" + deptype)
            dictkeylink = name + "-" + d['hash'] + "link"
            l = RpmLink(lcgcmt_version, name, version , platform, dirname, d['hash'])
            l.metadata = d['metadata']
            l.metadataFileName = d['metadataFileName']
            l.dependencies = list(d["dependencies"])
            for k, v in contriblinks.items():
                l.dependencies.append(v.name + "-link")
            l.dict = rpmDict
            rpmDict[dictkeylink] = l

    log.debug("Adding metadata package " + type)
    metarpm = RpmMetadata(lcgcmt_version, platform, type)
    if (metadataFileName != None):
        metarpm.sourceMetadataFileName = metadataFileName
    rpmDict["metadata"] = metarpm
    log.debug(list(rpmDict.keys()))

    # Now adding the contrib packages
    for v in contribs.values():
        rpmDict[v.name] = v
    for v in contriblinks.values():
        rpmDict[v.name+"-link"] = v
        
    log.debug("All packages added")    
    return rpmDict

###############################################################################
# Class to build the SPEC itself
###############################################################################

class RpmSpec(object):
    """ Class presenting the whole LCG spec """
    def __init__(self, version, platform, filename, lcg_dir, rpmroot, packageType, mainLCGFilename,
                 contribFileName, release, externalsre = None, rebuild_all=False):
        """ Initialize with the list of RPMs """
        self.lcgcmt_version = version
        self.lcgcmt_cmtconfig = platform
        self.release = release
        self.lcg_prefix = lcg_dir
        # Externals or generators
        self.packageType = packageType
        # Same info as boolean, easier to deal with later on...
        self.isMainLCG = (packageType == "externals")
        # options
        self.rebuild_all = rebuild_all

        # Now listing the required contribs
        contribData = loadContribMetaData(contribFileName)
        self.contribRPMs = {}
        self.contribRPMLinks = {}
        if contribData != None:
            for k, v in contribData.items():
                crpm = RpmContrib(self.lcgcmt_version, v["name"], v["version"], platform, v["dirname"], "")
                self.contribRPMs[v["name"]] = crpm
                log.warning("Adding dependency on contrib: %s %s %s)" % (v["name"], v["version"], crpm.config))
                                
                clrpm = RpmContribLink(self.lcgcmt_version, v["name"], v["version"], platform, v["dirname"], "")
                self.contribRPMLinks[v["name"]+ "-link"] = clrpm

        # Now loading the stuff
        tmpdata = loadLCGMetaData(filename)

        # Simple filtering by name, to avoid (re)producing RPMs already done for the main
        # file that is going to be loaded in the same hash
        self.rpmToBuild = [ n["name"] for n in tmpdata["packages"].values() ]
        self.rpmToBuild.append("metadata") 
        if not self.isMainLCG:
            # Then load it too
            log.info("Load Main LCG file: %s",  mainLCGFilename)
            lcgdata = loadLCGMetaData(mainLCGFilename)
            lcgpack = lcgdata["packages"]

            # Merging the two metadata fields to check the dependencies
            for k,v in lcgpack.items():
                tmpdata["packages"][k] = v

        # Merging the contrib as well
        for k,v in contribData.items():
            tmpdata["packages"][k] = v

        self.data = fixLCGCMTData(tmpdata)
        self.rpmDict = prepareRPMDict(self.data, self.lcgcmt_version,
                                      self.lcgcmt_cmtconfig, self.packageType,  filename,
                                      self.contribRPMs, self.contribRPMLinks)

        # Limit the list to the original list
        self.rpmList = [ v for v in self.rpmDict.values() if v.name in self.rpmToBuild or v.name == "metadata"]        
        # Plus the rpms with the links to the contrib
        for v in self.contribRPMLinks.values():
            self.rpmList.append(v)

        # Now filtering the externals
        if externalsre != None:
            import re
            tmplist = []
            log.warning("restricting packages to those matching: '%s'", externalsre )
            for r in self.rpmToBuild:
                if re.match(externalsre, r):
                    log.warning("Package matching regexp: %s", r)
                    tmplist.append(r)
            self.rpmToBuild = tmplist
        
        # Now reducing the list to the actual RPMs to build...
        tmplist = [ r for r in self.rpmList if r.name in self.rpmToBuild ]
            
        # Normally we only rebuild the "real" external package when the content is not behind a link,
        # we can force it with --rebuild-all for debug
        tmplist = [ r for r in tmplist if (r.checkDirExists(self.lcg_prefix)
                                           and (self.rebuild_all or not r.checkIfSymlink(self.lcg_prefix)) )
                    or r.name == "metadata" or isinstance(r, RpmLink) ]
        self.rpmList = tmplist
        for v in self.contribRPMLinks.values():
            self.rpmList.append(v)

        # printing final list
        log.info("Will produce RPMs for the following packages (%d):" % len(self.rpmList))
        log.info(sorted(self.rpmList, key=lambda x: x.name))        

        # Building the build dir paths
        myroot =  rpmroot
        self.topdir = "%s/rpmbuild" % myroot
        self.tmpdir = "%s/tmpbuild" % myroot
        self.rpmtmp = "%s/tmp" % myroot
        self.srcdir = os.path.join(self.topdir, "SOURCES")
        self.rpmsdir =  os.path.join(self.topdir, "RPMS")
        self.srpmsdir =  os.path.join(self.topdir, "SRPMS")
        self.builddir =  os.path.join(self.topdir, "BUILD")

        # And creating them if needed
        for d in [self.srcdir, self.rpmsdir, self.srpmsdir, self.builddir]:
            if not os.path.exists(d):
                os.makedirs(d)

        self.buildroot = os.path.join(self.tmpdir, "LCGCMT-%s-%s-buildroot" % \
                                      (self.lcgcmt_version, self.lcgcmt_cmtconfig))
        if not os.path.exists(self.buildroot):
            os.makedirs(self.buildroot)

        #self.externaldir = os.path.join(self.buildroot, "opt", "lhcb", "lcg", "external")
        #self.appreleasesdir = os.path.join(self.buildroot, "opt", "lhcb", "lcg", "app", "releases")

    def getLCGPath(self):
        """ Path for the installed files on the target system """
        return LCGPREFIX

    def getLCGVersionPath(self):
        """ Path LCG_<version> dir on the target system """
        return os.path.join(self.getLCGPath(), LCGNAME + "_" + version)
    
    def getHeader(self):
        """ Build the SPEC Header """
        # Ugly, should do this better
        projectName = "LCG"
        if type == "generators":
            projectName = "LCG_generators"
            
        rpm_header = Template("""
%define _binaries_in_noarch_packages_terminate_build   0
%define project $projectName
%define lbversion $rpmver
%define cmtconfig $rpmconfig
%define LCGCMTROOT $lcg_prefix

%define cmtconfig_rpm %( echo %{cmtconfig} | tr '-' '_' )
%define rpmversion %{lbversion}_%{cmtconfig_rpm}

%define _topdir $topdir
%define tmpdir $tmpdir
%define _tmppath $rpmtmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: %{project}_%{rpmversion}
Version: 1.0.0
Release: $release
Vendor: LHCb
Summary: %{project}
License: GPL
Group: LCG
Source0: %{url}
BuildRoot: %{tmpdir}/%{project}-%{lbversion}-%{cmtconfig}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: $rpmprefix
Provides: /bin/sh

""").substitute(rpmver=self.lcgcmt_version, rpmconfig=self.lcgcmt_cmtconfig, \
                topdir=self.topdir, tmpdir=self.tmpdir, rpmtmp=self.rpmtmp,
                lcg_prefix=self.lcg_prefix, projectName=projectName,
                release=self.release, rpmprefix=RPMPREFIX)
        return rpm_header

# RPM requiremenst for the whole package
#############################################################

    def getRequires(self):
        rpm_requires = ""
        for name in set([ r.getLCGRPMName() for r in self.rpmList]):
            rpm_requires += "Requires: %s\n" % name

        # Adding the contribs
        for c in self.contribRPMs.values():
            rpm_requires += "Requires: %s\n" % c.getLCGRPMName()
            
        return rpm_requires

# RPM Package section
#############################################################

    def getPackages(self):
        rpm_packages = ""
        for r in self.rpmList:
            rpm_packages += r.prepareRPMPackage()
        return rpm_packages

# RPM Description section
#############################################################

    def getDescriptions(self):
        rpm_desc = """
%description
%{project} %{lbversion}

"""
        for r in self.rpmList:
            rpm_desc += r.prepareRPMDescription()
        return rpm_desc

# return the post install actions
#############################################################

    def getPost(self):
        rpm_desc = ""
        for r in self.rpmList:
            rpm_desc += r.prepareRPMPost(self.getLCGPath())
        return rpm_desc

# RPM Common section with build
#############################################################

    def getCommon(self):
        rpm_common = '''
%prep

%build

%install

cd %_topdir/SOURCES

[ -d ${RPM_BUILD_ROOT} ] && rm -rf ${RPM_BUILD_ROOT}


'''
        for r in self.rpmList:
            if r.isMetaRPM:
                continue
            rpm_common += r.getInstallCommands(self.getLCGPath())
            if not r.checkDirExists(self.lcg_prefix):
                log.warning("MISSING: " + os.path.join(self.lcg_prefix, r.getSourcePath() ))
        rpm_common += '''

%post

%postun

%clean
'''
        return rpm_common

# RPM Files section
#############################################################

    def getFiles(self):
        rpm_files = """
%files

"""
        for r in self.rpmList:
            rpm_files += r.getFiles(self.getLCGPath())
        rpm_files += "\n"
        return rpm_files

# RPM Trailer
#############################################################

    def getTrailer(self):
        rpm_trailer = """
%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
"""
        return rpm_trailer


# Get the whole spec...
#############################################################

    def getSpec(self):
        """ Concatenate all the fragments """

        rpm_global = self.getHeader() \
        + self.getRequires() \
        + self.getPackages() \
        + self.getDescriptions() \
        + self.getCommon() \
        + self.getFiles() \
        + self.getPost() \
        + self.getTrailer()

        return rpm_global



###############################################################################
# Global utility methods
###############################################################################

# Extract LCG parameters from metadat file name
#############################################################
def getLCGParamsFromFileName(filename):
    """ Parse the filename to extract LCG information"""
    log.debug("Processing file %s" % filename)
    lcg_dir = os.path.realpath(os.path.dirname(filename))
    filename =  os.path.basename(filename)
    log.debug("LCG dir: %s" % lcg_dir)
    log.debug("Filename: %s" % filename)

    import re
    type = None
    cmtconfig = None
    m = re.match("^LCG_([a-zA-Z0-9]+)_(.*).txt$", filename)
    if m != None:
        type = m.group(1)
        cmtconfig = m.group(2)
        log.debug("Type: %s" % type)
        log.debug("platform: %s" % cmtconfig)

    # Trying to derive version from parent filename
    parentdirname =  os.path.basename(lcg_dir)
    version = None
    m2 = re.match("^LCG_(.*)$", parentdirname)
    if m2 != None:
        version = m2.group(1)
        log.debug("version: %s" % version)

    return (lcg_dir, filename, type, cmtconfig, version)


# Get Main LCG file name
#############################################################
def getMainLCGFileName(filename, version, platform):
    """ For generators, find main LCG file name,
    called LCG_externals_<platform>.txt"""
    lcg_dir = os.path.realpath(os.path.dirname(filename))
    return os.path.abspath(os.path.join(lcg_dir,
                                        "LCG_externals_%s.txt" % platform))

# Get Contrib file name
#############################################################
def getContribFileName(filename, platform):
    """ For generators, find main contrib file name,
    called LCG_contrib_<platform>.txt"""
    lcg_dir = os.path.realpath(os.path.dirname(filename))
    return os.path.join(lcg_dir, "LCG_contrib_%s.txt" % platform)



# Main method
#############################################################
def usage(cmd):
    """ Prints out how to use the script... """
    cmd = os.path.basename(cmd)
    return """\n%(cmd)s [options] LCG_description_filename

Prepare the SPEC file for a LCG release
Call with --help for details

    """ % { "cmd" : cmd }


###############################################################################
# Main method
###############################################################################
if __name__ == '__main__':
    # Setting logging 
    logging.basicConfig(stream=sys.stderr)

    # Parsing options
    parser = optparse.OptionParser(usage = usage(sys.argv[0]))
    parser.add_option('-d', '--debug',
                      dest="debug",
                      default=False,
                      action="store_true",
                      help="Show debug information")
    parser.add_option('-v', '--version',
                      dest="version",
                      default=None,
                      action="store",
                      help="Force LCG version")
    parser.add_option('-p', '--platform',
                      dest="platform",
                      default=None,
                      action="store",
                      help="Force platform")
    parser.add_option('-l', '--lcgdir',
                      dest="lcgdir",
                      default=None,
                      action="store",
                      help="Force LCG dir if different from the one containing the config file")
    parser.add_option('-b', '--buildroot',
                      dest="buildroot",
                      default="/tmp",
                      action="store",
                      help="Force build root")
    parser.add_option('-g', '--generators',
                      dest="generators",
                      default=False,
                      action="store_true",
                      help="Build generators instead of LCG")
    parser.add_option('-o', '--output',
                      dest="output",
                      default = None,
                      action="store",
                      help="File name for the generated specfile [default output to stdout]")
    parser.add_option('-m', '--mainLCGFile',
                      dest="mainLCGFile",
                      default=None,
                      action="store",
                      help="Specify the main LCG file, when building the generators")
    parser.add_option('--contribFile',
                      dest="contribFile",
                      default=None,
                      action="store",
                      help="Specify the contrib file, when building the generators")
    parser.add_option('--release',
                      dest="release",
                      default="1",
                      action="store",
                      help="Release number for the RPM")
    parser.add_option('--match',
                      dest="externalsre",
                      default=None,
                      action="store",
                      help="regular expression matching with the externals to be prepared")
    parser.add_option('--rebuild-all',
                      dest="rebuild_all",
                      default=False,
                      action="store_true",
                      help="Force rebuild of external RPM, even if the content is linked")

    opts, args = parser.parse_args(sys.argv)

    if len(args) != 2:
        parser.error("Please specify at least the file name")

    if opts.debug:
        log.setLevel(logging.DEBUG)
        
    input_filename = args[1]
    if not os.path.exists(input_filename):
        print("File: %s does NOT exist" % input_filename)
        sys.exit(1)

    # Parsing info from filename
    (fileLCGDir, filename, filenameType, filenamePlatform, filenameVersion) = getLCGParamsFromFileName(input_filename)
    (filePlatform, fileVersion) = loadLCGCMTVersionPlatform(input_filename)

    # The build root (default /tmp)
    rpmroot = opts.buildroot

    # Now selecting the parameters, either from options or from the parameters passed
    # Options override what was derived from the file name
    lcg_dir = fileLCGDir
    if opts.lcgdir != None:
        lcg_dir = opts.lcgdir

    types = ["externals", "generators" ]
    type = "externals"
    if filenameType != None and filenameType in types:
        type = filenameType
    if opts.generators:
        type = "generators"

    platform = filenamePlatform
    if filePlatform != None:
        platform = filePlatform
    if opts.platform != None:
        platform = opts.platform

    version = filenameVersion
    if fileVersion != None:
        version = fileVersion
    if opts.version != None:
        version = opts.version

    log.info("Processing %s" % input_filename)
    log.info("LCG Version: %s" % version)
    log.info("Platform: %s" % platform)
    log.info("Package type: %s" % type)
    log.info("LCG dir: %s" % lcg_dir)
    log.info("Build root: %s" % rpmroot)

    # Check if we build the externals or generators
    mainLCGFileName = None
    if type != "externals":
        if opts.mainLCGFile:
            mainLCGFileName = os.path.abspath(opts.mainLCGFile)
        else:
            mainLCGFileName = getMainLCGFileName(input_filename, version, platform)

    # Locate the file for the required contrib
    contribFileName = opts.contribFile
    if contribFileName == None:
        contribFileName = getContribFileName(input_filename, platform)
    
    spec = RpmSpec(version, platform, input_filename, lcg_dir, rpmroot, type, mainLCGFileName,
                   contribFileName, opts.release, opts.externalsre, opts.rebuild_all)

    if opts.output:
        with open(opts.output, "w") as outputfile:
            outputfile.write(spec.getSpec())
    else:
        print(spec.getSpec())
    log.info("Spec file generated")

