//----------------------------------------------------------------------------------------------------------------------
// This declarative Jenkins pipeline encodes all the steps required for the nightly of a single platform.
// Other jobs may call this pipelinbe to execute the build, test and installation of a set platforms.
//
// Author: Pere Mato
//----------------------------------------------------------------------------------------------------------------------

//---Global Functions and Maps------(specific to this pileline)---------------------------------------------------------
def lcg
def getCDashTrack() { return params.LCG_VERSION =~ 'dev' ? params.LCG_VERSION : 'Experimental'}
def getTarget() { return params.TARGET != 'all' ? params.TARGET : params.LCG_VERSION =~ 'ARM|BE|AdePT' ? 'top_packages' : 'all' }
def getIgnoreModules() { 
  modules = 'future#winreg jpype#jpypex ROOT#JsMVA importlib_resources#importlib_resources'
  if (params.LCG_VERSION =~ 'py3|python3|cuda|ARM|dev[3,4]$') modules += ' agile#AGILe gosam#golem QMtest#qm'
  if (params.LCG_VERSION =~ 'cuda') modules += ' hepdata_converter#hepdata_converter'
  if (params.TARGET != 'all') modules += ' ROOT#JupyROOT'
  if (params.LABEL =~ 'mac' ) modules += ' cartopy#cartopy  globus_sdk#globus_sdk  google_auth_oauthlib#google_auth_oauthlib paramiko#paramiko'
  if (params.LABEL =~ 'mac' ) modules += ' parsl#parsl PyJWT#jwt PyRDF#PyRDF qtpy#qtpy'
  return modules
}

pipeline {
  //---Global Parameters------------------------------------------------------------------------------------------------
  parameters {
    string(name:       'LCG_VERSION',          defaultValue: 'dev4cuda', description: 'LCG stack version to build')
    choice(name:       'COMPILER',             choices: ['gcc7', 'gcc8', 'gcc9', 'gcc10', 'clang8', 'clang10', 'native'])
    choice(name:       'BUILDTYPE',            choices: ['Release', 'Debug'])
    choice(name:       'LABEL',                choices: ['centos7', 'centos8', 'slc6', 'mac1015', 'arm64', 'ubuntu18', 'ubuntu20'])
    string(name:       'DOCKER_LABEL',         defaultValue: 'docker-host', description: 'Label for the the nodes able to launch docker images')
    string(name:       'ARCHITECTURE',         defaultValue: '', description: 'Complement of the architecture (instruction set)')

    string(name:       'LCG_INSTALL_PREFIX',   defaultValue: '/cvmfs/sft.cern.ch/lcg/latest:/cvmfs/sft.cern.ch/lcg/releases', description: 'Location to look for already installed packages matching version and hash value. Leave blank for full rebuilds')
    string(name:       'LCG_ADDITIONAL_REPOS', defaultValue: 'https://lcgpackages.web.cern.ch/tarFiles/latest', description: 'Additional binary repository')
    string(name:       'LCG_EXTRA_OPTIONS',    defaultValue: '-DLCG_SOURCE_INSTALL=OFF;-DLCG_TARBALL_INSTALL=ON', description: 'Additional configuration options to be added to the cmake command')
    string(name:       'LCG_IGNORE',           defaultValue: '', description: 'Packages already installed in LCG_INSTALL_PREFIX to be ignored (i.e. full rebuild is required). The list is \'single space\' separated.')
    booleanParam(name: 'USE_BINARIES',         defaultValue: true, description: 'Use binaries in repositories')
    string(name:       'TARGET',               defaultValue: 'all', description: 'Target to buuld (all, <package-name>, ...)')
    choice(name:       'BUILDMODE',            choices: ['nightly', 'release'], description: 'CDash mode')

    booleanParam(name: 'CLEAN_INSTALLDIR',     defaultValue: false, description: 'Instruct to clean the install directory before building')
    booleanParam(name: 'CVMFS_INSTALL',        defaultValue: true, description: '')
    booleanParam(name: 'RUN_TESTS',            defaultValue: true, description: '')
    booleanParam(name: 'VIEWS_CREATION',       defaultValue: true, description: '')
    string(name:       'CVMFS_DEPLOYER',       defaultValue: 'cvmfs-sft-nightlies', description: 'CVMFS publisher node label')
  }
  //---Options----------------------------------------------------------------------------------------------------------
  options {
    buildDiscarder(logRotator( daysToKeepStr: '14' ))
    timestamps()
  }
  //---Additional Environment Variables---------------------------------------------------------------------------------
  environment {
    CDASH_TRACK = getCDashTrack()
    CTEST_MODEL = 'Experimental'
    TARGET = getTarget()
    IGNORE_PYTHON_MODULE = getIgnoreModules()
  }

  agent none

  stages {
    //------------------------------------------------------------------------------------------------------------------
    //---Environment Stage to load and set global parameters------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    stage('Environment') {
      agent  { label "$DOCKER_LABEL"  }
      steps {
        script {
          //---Change the build name------------------------------------------------------------------------------------
          def BTYPE = [Release: 'opt', Debug: 'dbg']
          currentBuild.displayName = "#${BUILD_NUMBER}" + ' ' + params.LCG_VERSION + '-' + params.LABEL + '-' + params.COMPILER + '-' + BTYPE[params.BUILDTYPE]
          //---Load common variables and functions between several pilelines--------------------------------------------
          lcg = load 'lcgcmake/jenkins/Jenkinsfunctions.groovy'
        }
      }
    }
    //------------------------------------------------------------------------------------------------------------------
    //---Build stages---------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    stage('InDocker') {
      when {
        beforeAgent true
        expression { params.LABEL =~ 'centos|ubuntu|slc' }
      }
      agent {
        docker {
          alwaysPull true
          image "gitlab-registry.cern.ch/sft/docker/$LABEL"
          label "$DOCKER_LABEL"
          args  """-v /cvmfs:/cvmfs 
                   -v /ccache:/ccache 
                   -v /ec:/ec
                   -e SHELL
                   -e USER
                   -e container=docker
                   --net=host
                   --hostname ${LABEL}-docker
                """
        }
      }
      stages {
        stage('Build') {
          steps {
            script {
              lcg.buildPackages()
            }
          }
        }
        stage('CopyToEOS') {
          steps {
            script {
              lcg.copyToEOS()
            }
          }
        }
      }
    }
    stage('InBareMetal') {
      when {
        beforeAgent true
        expression { params.LABEL =~ 'arm64|mac' }
      }
      agent {
        label "$LABEL"
      }
      stages {
        stage('Build') {
          steps {
            script {
              lcg.buildPackages()
            }
          }
        }
        stage('CopyToEOS') {
          steps {
            script {
              lcg.copyToEOS()
            }
          }
        }
      }
    }
    //------------------------------------------------------------------------------------------------------------------
    //---CVMFS Installation stage---------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    stage('MacPreinstall') {
      when {
        beforeAgent true
        expression { params.CVMFS_INSTALL && params.LABEL =~ 'mac' }
      }
      agent {
        label "$LABEL"
      }
      steps {
        script {
          lcg.preinstallMacOS()
        }
      }
    }
    stage('CVMFSInstall') {
      when {
        beforeAgent true
        expression { params.CVMFS_INSTALL }
      }
      agent {
        label "$CVMFS_DEPLOYER"
      }
      steps {
        script {
          lcg.installCVMFS()
        }
      }
    }
    //------------------------------------------------------------------------------------------------------------------
    //---Testing stages-------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    stage('TestInDocker') {
      when {
        beforeAgent true
        expression { params.RUN_TESTS && 
                     params.LABEL =~ 'centos|ubuntu|slc' && 
                     !(params.LCG_VERSION =~ 'cuda') }
      }
      agent {
        docker {
          alwaysPull true
          image "gitlab-registry.cern.ch/sft/docker/$LABEL"
          label "$DOCKER_LABEL"
          args  """-v /cvmfs:/cvmfs 
                   -v /ccache:/ccache 
                   -v /ec:/ec
                   -e SHELL
                   -e USER
                   -e container=docker
                   --net=host
                   --hostname ${LABEL}-docker
                """
        }
      }
      stages {
        stage('WaitForCVMFS') {
          steps {
            script {
              lcg.waitForCVMFS()
            }
          }
        }
        stage('TestWithViews') {
          steps {
            script {
              lcg.cloneRepository('lcgtest', 'sft')
              lcg.testWithViews()
            }
          }
        }
        stage('TestShell') {
          steps {
            script {
              lcg.cloneRepository('lcg-shell-test')
              lcg.testShell()
            }
          }
        }
        stage('TestPythonImport') {
          steps {
            script {
              lcg.cloneRepository('lcg-import-test')
              lcg.testPythonImport()
            }
          }
        }
      }
    }
    stage('TestInBareMetal') {
      when {
        beforeAgent true
        expression { params.RUN_TESTS &&
                     ( params.LABEL =~ 'arm64|mac' ||
                       params.LCG_VERSION =~ 'cuda') }
      }
      agent {
        label "${lcg.testLABEL(params.LABEL, params.LCG_VERSION)}"
      }
      stages {
        stage('WaitForCVMFS') {
          steps {
            script {
              lcg.waitForCVMFS()
            }
          }
        }
        stage('TestWithViews') {
          steps {
            script {
              lcg.cloneRepository('lcgtest', 'sft')
              lcg.testWithViews()
            }
          }
        }
        stage('TestShell') {
          steps {
            script {
              lcg.cloneRepository('lcg-shell-test')
              lcg.testShell()
            }
          }
        }
        stage('TestPythonImport') {
          steps {
            script {
              lcg.cloneRepository('lcg-import-test')
              lcg.testPythonImport()
            }
          }
        }
      }
    }
  }
}
